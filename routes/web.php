<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// modulo1 gestion de perfil //admin
Route::get('user', 'UserController@index')->name('usuarios.index');
Route::get('user/create', 'UserController@create')->name('usuarios.create');
Route::get('user/{id}/', 'UserController@show')->name('usuarios.show');
Route::post('user/store', 'UserController@store')->name('usuarios.store');
Route::get('user/{id}/edit','UserController@edit' )->name('usuarios.edit');
Route::match(['put','patch'],'user/{id}', 'UserController@update')->name('usuarios.update');
Route::delete('user/{user}', 'UserController@destroy')->name('usuarios.destroy');
Route::get('style/{style}', 'UserController@change')->name('style');
Route::get('font/{font}', 'UserController@changeFont')->name('font');

Route::get('logout','Auth\LoginController@logout')->name('logout');

Route::get('mensaje', 'MensajeController@index')->name('mensaje.index');

//proveedor
Route::get('proveedor', 'ProveedorController@index')->name('proveedor.index');
Route::get('proveedor/create', 'ProveedorController@create')->name('proveedor.create');
Route::get('proveedor/{id}/', 'ProveedorController@show')->name('proveedor.show');
Route::post('proveedor/store', 'ProveedorController@store')->name('proveedor.store');
Route::get('proveedor/{id}/edit','ProveedorController@edit' )->name('proveedor.edit');
Route::match(['put','patch'],'proveedor/{id}', 'ProveedorController@update')->name('proveedor.update');
Route::delete('proveedor/{id}', 'ProveedorController@destroy')->name('proveedor.destroy');

//Cliente
Route::get('cliente', 'ClienteController@index')->name('cliente.index');
Route::get('cliente/create', 'ClienteController@create')->name('cliente.create');
Route::get('cliente/{id}/', 'ClienteController@show')->name('cliente.show');
Route::post('cliente/store', 'ClienteController@store')->name('cliente.store');
Route::get('cliente/{id}/edit','ClienteController@edit' )->name('cliente.edit');
Route::match(['put','patch'],'cliente/{nombre}', 'ClienteController@update')->name('cliente.update');
Route::delete('cliente/{cliente}', 'ClienteController@destroy')->name('cliente.destroy');

//empleado
Route::get('empleado', 'EmpleadoController@index')->name('empleado.index');
Route::get('empleado/create', 'EmpleadoController@create')->name('empleado.create');
Route::get('empleado/{id}/', 'EmpleadoController@show')->name('empleado.show');
Route::post('empleado/store', 'EmpleadoController@store')->name('empleado.store');
Route::get('empleado/{id}/edit','EmpleadoController@edit' )->name('empleado.edit');
Route::match(['put','patch'],'empleado/{nombre}', 'EmpleadoController@update')->name('empleado.update');
Route::delete('empleado/{empleado}', 'EmpleadoController@destroy')->name('empleado.destroy');
//Item
Route::get('item', 'ItemController@index')->name('item.index');
Route::get('item/create', 'ItemController@create')->name('item.create');
Route::get('item/{id}/', 'ItemController@show')->name('item.show');
Route::post('item/store', 'ItemController@store')->name('item.store');
Route::get('item/{id}/edit','ItemController@edit' )->name('item.edit');
Route::match(['put','patch'],'item/{id}', 'ItemController@update')->name('item.update');
Route::delete('item/{id}', 'ItemController@destroy')->name('item.destroy');

//trabajo---1
Route::get('trabajo', 'TrabajoController@index')->name('trabajo.index');
Route::get('trabajo/create/{id}/', 'TrabajoController@create')->name('trabajo.create');
Route::get('trabajo/{id}/', 'TrabajoController@show')->name('trabajo.show');
Route::post('trabajo/store', 'TrabajoController@store')->name('trabajo.store');
Route::get('trabajo/{id}/edit','TrabajoController@edit' )->name('trabajo.edit');
Route::match(['put','patch'],'trabajo/{id}', 'TrabajoController@update')->name('trabajo.update');
Route::delete('trabajo/{id}', 'TrabajoController@destroy')->name('trabajo.destroy');

//Ingreso
Route::get('ingreso', 'IngresoController@index')->name('ingreso.index');
Route::get('ingreso/create', 'IngresoController@create')->name('ingreso.create');
Route::get('ingreso/{id}/', 'IngresoController@show')->name('ingreso.show');
Route::post('ingreso/store', 'IngresoController@store')->name('ingreso.store');
Route::get('ingreso/{id}/edit','IngresoController@edit' )->name('ingreso.edit');
Route::match(['put','patch'],'ingreso/{id}', 'IngresoController@update')->name('ingreso.update');
Route::delete('ingreso/{id}', 'IngresoController@destroy')->name('ingreso.destroy');



//detallepresupuesto
Route::get('detallepresupuesto', 'DetallepresupuestoController@index')->name('detallepresupuesto.index');
Route::get('detallepresupuesto/create/{id}/', 'DetallepresupuestoController@create')->name('detallepresupuesto.create');
Route::get('detallepresupuesto/{id}/', 'DetallepresupuestoController@show')->name('detallepresupuesto.show');
Route::post('detallepresupuesto/store', 'DetallepresupuestoController@store')->name('detallepresupuesto.store');
Route::get('detallepresupuesto/{id}/edit','DetallepresupuestoController@edit' )->name('detallepresupuesto.edit');
Route::match(['put','patch'],'detallepresupuesto/{id}', 'DetallepresupuestoController@update')->name('detallepresupuesto.update');
Route::delete('detallepresupuesto/{id}', 'DetallepresupuestoController@destroy')->name('detallepresupuesto.destroy');

//factura
Route::get('factura', 'FacturaController@index')->name('factura.index');
Route::get('factura/showFacturas', 'FacturaController@showFacturas')->name('factura.showFacturas');
Route::get('factura/create/{id}/', 'FacturaController@create')->name('factura.create');
Route::get('factura/{id}/', 'FacturaController@show')->name('factura.show');
Route::post('factura/store', 'FacturaController@store')->name('factura.store');
Route::get('factura/{id}/edit','FacturaController@edit' )->name('factura.edit');
Route::match(['put','patch'],'factura/{id}', 'FacturaController@update')->name('factura.update');
Route::delete('factura/{id}', 'FacturaController@destroy')->name('factura.destroy');

Route::get('reporte', 'ReporteController@index')->name('reporte.index');
Route::get('/reporte1','ReporteController@reporte1')->name('reporte.1');
Route::get('/reporte2','ReporteController@reporte2')->name('reporte.2');
Route::get('/reporte3','ReporteController@Amistades')->name('reporte.3');
// modulo8 estadisticas
Route::get('estadistica', 'EstadisticaController@index')->name('estadistica.index');
Route::get('/estadistica1','EstadisticaController@estadistica1')->name('estadistica.1');
Route::get('/estadistica2','EstadisticaController@estadistica2')->name('estadistica.2');
Route::get('/estadistica3','EstadisticaController@estadistica3')->name('estadistica.3');

//grupoes
Route::get('grupo', 'GrupoController@index')->name('grupo.index');
Route::get('grupo/create', 'GrupoController@create')->name('grupo.create');
Route::get('grupo/{id}/', 'GrupoController@show')->name('grupo.show');
Route::post('grupo/store', 'GrupoController@store')->name('grupo.store');
Route::get('grupo/{id}/edit','GrupoController@edit' )->name('grupo.edit');
Route::match(['put','patch'],'grupo/{id}', 'GrupoController@update')->name('grupo.update');
Route::delete('grupo/{id}', 'GrupoController@destroy')->name('grupo.destroy');

//moduloes
Route::get('modulo', 'ModuloController@index')->name('modulo.index');
Route::get('modulo/create', 'ModuloController@create')->name('modulo.create');
Route::get('modulo/{id}/', 'ModuloController@show')->name('modulo.show');
Route::post('modulo/store', 'ModuloController@store')->name('modulo.store');
Route::get('modulo/{id}/edit','ModuloController@edit' )->name('modulo.edit');
Route::match(['put','patch'],'modulo/{id}', 'ModuloController@update')->name('modulo.update');
Route::delete('modulo/{id}', 'ModuloController@destroy')->name('modulo.destroy');

//menues
Route::get('menu', 'MenuController@index')->name('menu.index');
Route::get('menu/create', 'MenuController@create')->name('menu.create');
Route::get('menu/{id}/', 'MenuController@show')->name('menu.show');
Route::post('menu/store', 'MenuController@store')->name('menu.store');
Route::get('menu/{id}/edit','MenuController@edit' )->name('menu.edit');
Route::match(['put','patch'],'menu/{id}', 'MenuController@update')->name('menu.update');
Route::delete('menu/{id}', 'MenuController@destroy')->name('menu.destroy');

//grupomenues
Route::get('grupomenu', 'GrupoMenuController@index')->name('grupomenu.index');
Route::get('grupomenu/create', 'GrupoMenuController@create')->name('grupomenu.create');
Route::get('grupomenu/{id}/', 'GrupoMenuController@show')->name('grupomenu.show');
Route::post('grupomenu/store', 'GrupoMenuController@store')->name('grupomenu.store');
Route::get('grupomenu/{id}/edit','GrupoMenuController@edit' )->name('grupomenu.edit');
Route::match(['put','patch'],'grupomenu/{id}', 'GrupoMenuController@update')->name('grupomenu.update');
Route::delete('grupomenu/{id}', 'GrupoMenuController@destroy')->name('grupomenu.destroy');
//cargoes
Route::get('cargo', 'CargoController@index')->name('cargo.index');
Route::get('cargo/create', 'CargoController@create')->name('cargo.create');
Route::get('cargo/{id}/', 'CargoController@show')->name('cargo.show');
Route::post('cargo/store', 'CargoController@store')->name('cargo.store');
Route::get('cargo/{id}/edit','CargoController@edit' )->name('cargo.edit');
Route::match(['put','patch'],'cargo/{id}', 'CargoController@update')->name('cargo.update');
Route::delete('cargo/{id}', 'CargoController@destroy')->name('cargo.destroy');

//asignacion
Route::get('asignacion', 'AsignacionController@index')->name('asignacion.index');
Route::get('asignacion/create', 'AsignacionController@create')->name('asignacion.create');
Route::get('asignacion/{id}/', 'AsignacionController@show')->name('asignacion.show');
Route::post('asignacion/store', 'AsignacionController@store')->name('asignacion.store');
Route::get('asignacion/{id}/edit','AsignacionController@edit' )->name('asignacion.edit');
Route::match(['put','patch'],'asignacion/{id}', 'AsignacionController@update')->name('asignacion.update');
Route::delete('asignacion/{id}', 'AsignacionController@destroy')->name('asignacion.destroy');

//almacenes
Route::get('almacen', 'AlmacenController@index')->name('almacen.index');
Route::get('almacen/create', 'AlmacenController@create')->name('almacen.create');
Route::get('almacen/{id}/', 'AlmacenController@show')->name('almacen.show');
Route::post('almacen/store', 'AlmacenController@store')->name('almacen.store');
Route::get('almacen/{id}/edit','AlmacenController@edit' )->name('almacen.edit');
Route::match(['put','patch'],'almacen/{id}', 'AlmacenController@update')->name('almacen.update');
Route::delete('almacen/{id}', 'AlmacenController@destroy')->name('almacen.destroy');

//ordentrabajo
Route::get('ordentrabajo', 'OrdenTrabajoController@index')->name('ordentrabajo.index');
Route::get('ordentrabajo/create', 'OrdenTrabajoController@create')->name('ordentrabajo.create');
Route::get('ordentrabajo/{id}/', 'OrdenTrabajoController@show')->name('ordentrabajo.show');
Route::post('ordentrabajo/store', 'OrdenTrabajoController@store')->name('ordentrabajo.store');
Route::get('ordentrabajo/{id}/edit','OrdenTrabajoController@edit' )->name('ordentrabajo.edit');
Route::match(['put','patch'],'ordentrabajo/{id}', 'OrdenTrabajoController@update')->name('ordentrabajo.update');
Route::delete('ordentrabajo/{id}', 'OrdenTrabajoController@destroy')->name('ordentrabajo.destroy');
Route::get('/asignar', 'OrdenTrabajoController@asignar')->name('ordentrabajo.asignar');
Route::post('ordentrabajo/asignarOT', 'OrdenTrabajoController@asignarOT')->name('ordentrabajo.asignarOT');

Route::get('ordentrabajo/{id}/cerrar','OrdenTrabajoController@cerrar' )->name('ordentrabajo.cerrar');
Route::post('ordentrabajo/cerrarOT', 'OrdenTrabajoController@cerrarOT')->name('ordentrabajo.cerrarOT');




Route::get('solicitud', 'SolicitudController@index')->name('solicitud.index');
Route::get('solicitud/create', 'SolicitudController@create')->name('solicitud.create');
Route::get('solicitud/{id}/', 'SolicitudController@show')->name('solicitud.show');
Route::post('solicitud/store', 'SolicitudController@store')->name('solicitud.store');
Route::get('solicitud/{id}/edit','SolicitudController@edit' )->name('solicitud.edit');
Route::match(['put','patch'],'solicitud/{id}', 'SolicitudController@update')->name('solicitud.update');
Route::delete('solicitud/{id}', 'SolicitudController@destroy')->name('solicitud.destroy');
//traspaso de materiales
Route::get('/traspaso', 'SolicitudController@traspaso')->name('solicitud.traspaso');
Route::post('Solicitud/traspasoItem', 'SolicitudController@traspasoItem')->name('solicitud.traspasoItem');
//ingreso de equipos
Route::get('/ingreso', 'SolicitudController@ingreso')->name('solicitud.ingreso');
Route::post('Solicitud/ingresostore', 'SolicitudController@ingresostore')->name('solicitud.ingresostore');