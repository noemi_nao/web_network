<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    { 
      $datos = [
          ['id'=>'1', 'documento'=>'123456', 'nombre'=>'Juan', 'apellidop'=>'Perez', 'apellidom'=>'Peres', 'fechanac'=>'01/01/1990', 'sexo'=>'F', 'telefono'=>'77668899', 'direccion'=>'Calle A S/N', 'estado'=>'N'],
      ];
      foreach($datos as $dato){
        \App\Cliente::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'GRUPO MAYOR'],
      ];
      foreach($datos as $dato){
        \App\Grupo::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'CARGO 1', 'observacion'=>'No seque observacion'],
      ];
      foreach($datos as $dato){
        \App\Cargo::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Regional 1'],
      ];
      foreach($datos as $dato){
        \App\Regional::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Area 1'],
      ];
      foreach($datos as $dato){
        \App\Area::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Localidad 1', 'area_id'=>'1', 'regional_id'=>'1' ],
      ];
      foreach($datos as $dato){
        \App\Localidad::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Normal' ],
      ];
      foreach($datos as $dato){
        \App\TipoAlmacen::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Demora 1' ],
      ];
      foreach($datos as $dato){
        \App\JustificacionDemora::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Tipo Orden 1' ],
      ];
      foreach($datos as $dato){
        \App\TipoOrden::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Tipo Item 1' ],
      ];
      foreach($datos as $dato){
        \App\TipoItem::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Medida 1' ],
      ];
      foreach($datos as $dato){
        \App\Medida::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Marca 1' ],
      ];
      foreach($datos as $dato){
        \App\Marca::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Tipo Solicitud 1' ],
      ];
      foreach($datos as $dato){
        \App\TipoSolicitud::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Almacen 1', 'estado'=>'A', 'direccion'=>'Calle B 1337', 'tipoalmacen_id'=>'1', 'localidad_id'=>'1' ],
      ];
      foreach($datos as $dato){
        \App\Almacen::create($dato);
      }
      $datos = [
          ['id'=>'1', 'documento'=>'12345', 'nombre'=>'Carlos', 'apellidop'=>'Villagran', 'apellidom'=>'Quico', 'fechanac'=>'01/01/1990', 'sexo'=>'M', 'telefono'=>'222226268', 'direccion'=>'Calle Grande Esq Calle Chica', 'estado'=>'N', 'cargo_id'=>'1'   ],
      ];
      foreach($datos as $dato){
        \App\Empleado::create($dato);
      }
      $datos = [
          ['id'=>'1', 'nombre'=>'Napoleon Bonaparte', 'nit'=>'876541015', 'correo'=>'pedidos@grancastillo.com', 'direccion'=>'Calle Bastilla 1759', 'telefono'=>'6689548', 'estado'=>'N', ],
      ];
      foreach($datos as $dato){
        \App\Proveedor::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Item 1', 'observacion'=>'Ropa Roja', 'estado'=>'B', 'umbral'=>'2', 'foto'=>'ropa.jpg' , 'tipoitem_id'=>'1', 'medida_id'=>'1', 'marca_id'=>'1', 'proveedor_id'=>'1' ],
      ];
      foreach($datos as $dato){
        \App\Item::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'CUS 1', 'ruta'=>'Ruta 1' ],
      ];
      foreach($datos as $dato){
        \App\Cu::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Modulo 1', 'ruta'=>'Ruta 1' ],
      ];
      foreach($datos as $dato){
        \App\Modulo::create($dato);
      }
      $datos = [
          ['id'=>'1', 'nombre'=>'Menu 1', 'ruta'=>'Ruta 1' , 'descripcion'=>'Descripcion del menu 1', 'modulo_id'=>'1' ],
      ];
      foreach($datos as $dato){
        \App\Menu::create($dato);
      }
      $datos = [
          ['id'=>'1', 'estado'=>'N', 'grupo_id'=>'1' , 'menu_id'=>'1' ],
          ['id'=>'2', 'estado'=>'N', 'grupo_id'=>'1' , 'menu_id'=>'1' ],
      ];
      foreach($datos as $dato){
        \App\GrupoMenu::create($dato);
      }
      $datos = [
            ['id'=>'1', 'email'=>'correo@correo.com', 'password'=>bcrypt('123'), 'name'=>'Administrador', 'estado'=>true,
             'estilo'=>'blue', 'fuente'=>'15', 'email_verified_at'=>date('Y-m-d H:i:s'), 'admin'=>true,'imagen'=>'img.jpg', 'grupo_id'=>'1', 'empleado_id'=>'1', 'almacen_id'=>'1', ],
      ];
      foreach($datos as $dato){
        \App\User::create($dato);
      }
      $datos = [
        ['id'=>'1', 'telefono1'=>'77668899', 'postacion'=>'1', 'cruce'=>'0', 'resultado'=>'NoseRes',
        'fechaCreacion'=>date('Y-m-d H:i:s'), 'fechaRecepcion'=>date('Y-m-d H:i:s') , 'fechaInicio'=>date('Y-m-d H:i:s'),
        'fechaFin'=>date('Y-m-d H:i:s'), 'observacion'=>'Observacion 1', 'estado'=>'N','cliente_id'=>'1',
        'usuario_id'=>'1', 'localidad_id'=>'1', 'tipoorden_id'=>'1', 'almacen_id'=>'1', 'justificacion_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\ordentrabajo::create($dato);
      }
      $datos = [
          ['id'=>'1', 'descripcion'=>'Solicitud 1', 'fechasolicitud'=>'2021-01-01', 'fechaaprobacion'=>'2021-01-01', 'estado'=>'N', 'usuario_id'=>'1', 'almacenorigen_id'=>'1', 'almacendestino_id'=>'1', 'tiposolicitud_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\solicitud::create($dato);
      }
      $datos = [
          ['id'=>'1', 'serie'=>'126465-1263', 'estado'=>'N', 'cantidad'=>'10','instalado'=>'C','precio'=>'12.5', 'solicitud_id'=>'1', 'item_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\DetalleSolicitud::create($dato);
      }
      $datos = [
          ['id'=>'1', 'serie'=>'126465-1263', 'cantidad'=>'10', 'instalado'=>'C','almacen_id'=>'1','item_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\AlmacenItem::create($dato);
      }
      $datos = [
          ['id'=>'1', 'serie'=>'126465-1263', 'cantidad'=>'8', 'orden_id'=>'1','item_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\ItemUtilizado::create($dato);
      }
      $datos = [
          ['id'=>'1', 'observacion'=>'Item Que se utiliza', 'resultado'=>'B', 'estado'=>'N', 'fecha'=>'2021-05-19', 'ordentrabajo_id'=>'1', 'usuario_id'=>'1'],
      ];
      foreach($datos as $dato){
        \App\controlcalidad::create($dato);
      }



    }
}
