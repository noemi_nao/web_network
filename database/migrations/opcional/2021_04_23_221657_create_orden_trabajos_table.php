<?php
/*
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenTrabajosTable extends Migration
{

    public function up()
    {
        Schema::create('orden_trabajos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('telefono1',8);
            $table->integer('postacion');
            $table->integer('cruce');
            $table->string('resultado');
            $table->string('fechaCreacion');
            $table->string('fechaRecepcion');
            $table->string('fechaInicio');
            $table->string('fechaFin');
            $table->string('observacion');
            $table->string('estado',1);
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
            $table->integer('empleado_id')->unsigned();
            $table->foreign('empleado_id')->references('id')->on('empleados')->onDelete('cascade');
            $table->integer('localidad_id')->unsigned();
            $table->foreign('localidad_id')->references('id')->on('localidads')->onDelete('cascade');
            $table->integer('tipoorden_id')->unsigned();
            $table->foreign('tipoorden_id')->references('id')->on('tipo_ordens')->onDelete('cascade');
            $table->integer('almacen_id')->nullable()->unsigned();
            $table->foreign('almacen_id')->references('id')->on('almacens')->onDelete('cascade');
            $table->integer('justificacion_id')->nullable()->unsigned();
            $table->foreign('justificacion_id')->references('id')->on('justificacion_demoras')->onDelete('cascade');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('orden_trabajos');
    }
}

*/
