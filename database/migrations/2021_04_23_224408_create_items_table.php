<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion'); 
            $table->string('observacion');
            $table->string('estado',1);
            $table->integer('umbral');
            $table->string('foto')->nullable();
            $table->integer('tipoitem_id')->unsigned();
            $table->foreign('tipoitem_id')->references('id')->on('tipo_items')->onDelete('cascade');
            $table->integer('medida_id')->unsigned();
            $table->foreign('medida_id')->references('id')->on('medidas')->onDelete('cascade');
            $table->integer('marca_id')->unsigned();
            $table->foreign('marca_id')->references('id')->on('marcas')->onDelete('cascade');
            $table->integer('proveedor_id')->unsigned();
            $table->foreign('proveedor_id')->references('id')->on('proveedors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
