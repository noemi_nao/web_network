<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicituds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('fechasolicitud');
            $table->string('fechaaprobacion')->nullable();
            $table->string('estado',1);
            $table->bigInteger('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('almacenorigen_id')->nullable()->unsigned();
            $table->foreign('almacenorigen_id')->references('id')->on('almacens')->onDelete('cascade');
            $table->integer('almacendestino_id')->nullable()->unsigned();
            $table->foreign('almacendestino_id')->references('id')->on('almacens')->onDelete('cascade');
            $table->integer('tiposolicitud_id')->unsigned();
            $table->foreign('tiposolicitud_id')->references('id')->on('tipo_solicituds')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}
