<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemUtilizadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_utilizados', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('serie',16)->nullable();
            $table->integer('cantidad')->nullable();
            $table->integer('orden_id')->unsigned();
            $table->foreign('orden_id')->references('id')->on('ordentrabajos')->onDelete('cascade'); 
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_utilizados');
    }
}
