<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdentrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordentrabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('telefono1',8);
            $table->integer('postacion')->default('0');
            $table->integer('cruce')->default('0');
            $table->string('resultado')->nullable();
            $table->string('fechaCreacion');
            $table->string('fechaRecepcion')->nullable();
            $table->string('fechaInicio')->nullable();
            $table->string('fechaFin')->nullable();
            $table->string('observacion')->nullable();
            $table->string('estado',1);
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');

            $table->bigInteger('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('localidad_id')->unsigned();
            $table->foreign('localidad_id')->references('id')->on('localidads')->onDelete('cascade');
            $table->integer('tipoorden_id')->unsigned();
            $table->foreign('tipoorden_id')->references('id')->on('tipo_ordens')->onDelete('cascade');
            $table->integer('almacen_id')->nullable()->unsigned();
            $table->foreign('almacen_id')->references('id')->on('almacens')->onDelete('cascade');
            $table->integer('justificacion_id')->nullable()->unsigned();
            $table->foreign('justificacion_id')->references('id')->on('justificacion_demoras')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordentrabajos');
    }
}
