@extends('layouts.layout')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Ingresos de Material</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>    
</h1>
<a class="btn btn-success" href="{{ route('ingreso.create')}}">Create</a>

@empty ($ingresos)
    <div class="alert alert-warning">
         No existen Ingresos realizados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>PROVEEDOR</th>
                <th>ITEM</th>
                <th>CANTIDAD</th>
                <th>FECHA</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($ingresos as $ingreso)
            <tr>
                <td>{{ $ingreso->id}}</td>
                <td>{{ $ingreso->proveedores}}</td>
                <td>{{ $ingreso->items}}</td>
                <td>{{ $ingreso->cantidad}}</td>
                <td>{{ $ingreso->fecha}}</td>
                <td>      
               
                     <form method="POST" class="d-inline" action="{{ route('ingreso.destroy',$ingreso->id )}}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
@endsection
