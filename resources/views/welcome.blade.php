<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Network Vision</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
body {
    color: #646464;
    background: #e2e2e2;
}	
.login-form {
    width: 300px;
    margin: 30px auto;
}
.login-form h2 {
    font-size: 26px;
    font-weight: bold;
    margin: 30px 0;
    text-align: center;
}
.login-form form {
    color: #fff;
    background: #c2970b;
    background: radial-gradient(circle, navy,#FFFFFF);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    padding: 25px;
    margin-bottom: 15px;
}
.login-form .form-control {
    border-color: #dfdfdf;
    box-shadow: none !important;
}
.login-form .form-control, .login-form .btn {
    min-height: 38px;        
}
.login-form input[type="email"] {
    border-radius: 2px 2px 0 0;
}
.login-form input[type="password"] {
    border-radius: 0 0 2px 2px;
    margin-top: -1px;
}    
.login-form .btn, .login-form .btn:active {        
    font-size: 15px;
    font-weight: bold;
    border-radius: 2px;
    background: #eeeeee !important;
    color: #646464;
    margin-bottom: 25px;
}
.login-form .btn:hover, .login-form .btn:focus{
    background: #e4e4e4 !important;
}
.login-form a {		
    color: #405e9e;
}
.login-form form a {
    color: #fff;
}
</style>
</head>
<body>
<div class="login-form">

    <form method="POST" action="{{ route('login') }}">
        <td class="text-center">
            <div>
                <img src="{{asset('/images/logo/logo_datacom.jpg')}}" width="200">
            </div>
        </td>
        <h2 class="text-center">DATACOM S.R.L. Login</h2>
        @csrf
        <div class="form-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Correo Electronico">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-secondary btn-block">Entrar</button>
        </div>
        <p class="text-center"><a href="#">Bienvenido a Datacom S.R.L.</a></p>     
    </form>
 
    <p class="text-center small">No Esta Registrado?<a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
    </p>
    
</div>
</body>
</html>