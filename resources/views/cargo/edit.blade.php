@extends('layouts.layout')

@section('title','Modificacion cargos')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<h1>Modificacion de cargos</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif


    <form class="form-group" action="{{ route('cargo.update', $cargo->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
    <div class="card-body">
     <form method="POST" action="{{ route('cargo.store') }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <h1>Edicion de cargos</h1>     
 
         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
 
             <div class="col-md-6">
                 <input id="descripcion" type="text" value = "{{$cargo->descripcion}}" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion">
             @error('descripcion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>

           <div class="form-group row">
             <label for="observacion" class="col-md-4 col-form-label text-md-right">{{ __('Observacion') }}</label>
 
             <div class="col-md-6">
                 <input id="observacion" type="text" value = "{{$cargo->observacion}}" class="form-control @error('observacion') is-invalid @enderror" name="observacion" value="{{ old('observacion') }}" required autocomplete="observacion">
             @error('observacion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
        
     
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
                 <button type="submit" class="btn btn-primary">
                     {{ __('Editar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{ url('/home') }}">Cancelar</a>
             </div>
         </div>
        
     </form>
 </div>


@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection