@extends('layouts.layout')

@section('title','Registro grupomenu')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de grupomenus</h1>
</font>
        
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('grupomenu.store') }}" enctype="multipart/form-data">
         @csrf      
            <font size=32 style="color:#132735" face="Segoe UI">
               <h1 align="center">Registro de grupomenus</h1>
            </font>
        
          <div class="form-row">
          <label for="grupo_id" class="col-md-4 col-form-label text-md-right">{{ __('Grupo') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="grupo_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Grupo->all() as $grupo)
                                   <option value="{{$grupo->id}}">{{$grupo->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('grupo_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
                 <div class="form-row">
          <label for="menu_id" class="col-md-4 col-form-label text-md-right">{{ __('Menu') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="menu_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Menu->all() as $menu)
                                   <option value="{{$menu->id}}">{{$menu->nombre}}</option>
                              @endforeach          
                    </select>
                    @error('menu_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
         <!--  datos añadidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
