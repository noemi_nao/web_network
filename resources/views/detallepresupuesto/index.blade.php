@extends('layouts.layout')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Presupuestos pendientes de atencion</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>    
</h1>


@empty ($detallepresupuestos)
    <div class="alert alert-warning">
         No existen presupuestos pendientes
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>DESCRIPCION</th>
                <th>CLIENTE</th>
                <th>FECHA DE SOLICITUD</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detallepresupuestos as $detallepresupuesto)
            <tr>
                <td>{{$detallepresupuesto->id}}</td>
                <td>{{$detallepresupuesto->descripcion}}</td>
                <td>{{$detallepresupuesto->cliente}}</td>
                <td>{{$detallepresupuesto->fechasolicitud}}</td>
                <td>      
                    <a class="btn btn-success" href="{{ route('detallepresupuesto.create',$detallepresupuesto->id)}}">Create</a>
                    <a href="{{ route('detallepresupuesto.show',$detallepresupuesto->id)}}" class="btn btn-secondary">Atender</a>  
                    <a class="btn btn-primary" 
                      href="{{ route('detallepresupuesto.edit',$detallepresupuesto->id)}}">editar</a>
                     <form method="POST" class="d-inline" action="{{ route('detallepresupuesto.destroy',$detallepresupuesto->id )}}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@section('script')
@endsection
@endsection
@endsection
