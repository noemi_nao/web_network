@extends('layouts.layout')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">solicituds</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>    
</h1>
<a class="btn btn-success" href="{{ route('solicitud.create')}}">Create</a>

@empty ($Solicituds)
    <div class="alert alert-warning">
         No existen solicituds Creados
    </div>
@else
<div class="table-responsive">
    <h1>Listado de solicituds</h1>
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>DESCRIPCION</th>
                <th>ESTADO</th>
         
            </tr>
        </thead>
        <tbody>
            @foreach ($Solicituds as $solicitud)
            <tr>
                <td>{{ $solicitud->id}}</td>
                <td>{{ $solicitud->descripcion}}</td>
                <td>{{ $solicitud->estado}}</td>
              
                <td>      

                    <a href="{{ route('solicitud.show',$solicitud->id)}}" class="btn btn-secondary">Ver Detalle</a>  
            
                    <a class="btn btn-primary" href="{{ route('solicitud.edit',$solicitud->id)}}">Aprobar</a>
           
                    <form method="POST" class="d-inline" action="{{ route('solicitud.destroy',$solicitud->id )}}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Rechazar</button>
                      </form>
        
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
   
  </div>
</div>
@endsection
@endsection
