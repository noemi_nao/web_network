@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de solicituds</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="panel panel-primary">
<div class="panel-body">
     <form method="POST" action="{{ route('solicitud.store') }}" enctype="multipart/form-data">
         @csrf
            <div class="col-md-6">
          <div class="form-group row">
            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('descripcion') }}</label>
                <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="" required autocomplete="descripcion" autofocus>
                @error('descripcion')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
     
        <div class="col-md-4">
         <div class="form-group row">
            <label for="item" class="col-md-4 col-form-label text-md-right">{{ __('item') }}</label>
            <select name="iditem" id="iditem" class="form-control selectpicker" data-live-search="true">
                @foreach ($items as $item)
                    <option value="{{$item->id}}">{{$item->descripcion}}-{{$item->nombre}}</option>          
                @endforeach
            </select>
        </div>
    </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label for="idcantidad" class="col-md-2 col-form-label text-md-right">{{ __('Cantidad') }}</label>              
                        <input id="idcantidad" type="text" class="form-control @error('idcantidad') is-invalid @enderror" name="idcantidad" value="{{ old('idcantidad') }}" required autocomplete="idcantidad" autofocus>
                        @error('idcantidad')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                <div class="form-group row">
                    <label for="idprecio" class="col-md-2 col-form-label text-md-right">{{ __('Precio') }}</label>              
                        <input id="idprecio" type="text" class="form-control @error('idprecio') is-invalid @enderror" name="idprecio" value="{{ old('idprecio') }}" required autocomplete="idprecio" autofocus>
                        @error('idprecio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
     <div class="col-md-3 offset-md-4"> 
        <div class="form-group row">
            <button type="button" id="bt_add" class="btn btn-primary">{{ __('Agregar') }}</button>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
            <thead style="background-color:#A9D0F5">
                <th>OPCIONES</th>
                <th>ITEM</th>
                <th>CANTIDAD</th>
                <th>PRECIO</th>
                <th>SUB TOTAL</th>
            </thead>
            <tfoot>
                <th>TOTAL</th>
                <th></th>
                <th></th>
                <th></th>
                <th><h4 id="total">0</h4></th>
            </tfoot>
            <tbody>
            </tbody>
        </table>  
    </div>
         <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
         <input name="_token" value="{{csrf_token()}}" type="hidden">
         <div class="form-group row">        
                 <button class="btn btn-primary" type="submit">{{ __('Guardar') }}</button>
             </div>
         </div>
     </form>
 </div>
</div>
@section('footer')
<div class="alert alert-primary" role="alert">

</div>
@push('scripts')
<script>
  $(document).ready(function(){
    $('#bt_add').click(function(){
      agregar();
    });
});
  var cont=0;
  total=0;
  subtotal=[];
  $("#guardar").hide();

  
  function limpiar(){
    $("#cantidad").val("");
    $("#precio").val("");
  }

  

  function evaluar()
  {
    if (total>0)
    {
      $("#guardar").show();
    }
    else
    {
      $("#guardar").hide(); 
    }
   }



  function agregar(){
  idarticulo=$("#iditem").val();
  articulo=$("#iditem option:selected").text();
  cantidad= $("#idcantidad").val();
  precio=$("#idprecio").val();
  if(iditem!="" && cantidad!="" && precio!="" && cantidad>0 && precio>0){
    subtotal[cont]=precio*cantidad;
    total=total+subtotal[cont];
    var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td><input type="number" name="precio[]" value="'+precio+'"></td><td>'+subtotal[cont]+'</td></tr>';
  cont++;
  limpiar();
  $("#total").html(" "+total);
  evaluar();
  $("#detalles").append(fila);
  }
  else{
      alert("error al ingresar detalle de presupuesto");
  }
  }

  function eliminar(index){
    total=total-subtotal[index]; 
    $("#total").html("Bs/ " + total);   
    $("#fila" + index).remove();
    evaluar();
  }


</script>
@endpush
@endsection
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection