@extends('layouts.layout')

@section('title','solicitud')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Detalles solicitud</h1>
</font>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>
         
     @endif
           <div class="text-center"> 
            <!--  <h5 class="card-title">{{--$solicitud->nombre--}} </h5> -->
   
            <table class="table table-striped" >
                <thead class="thrad-light">
                    <tr>
                        <th>ID</th>
                        <th>ITEM</th>
                        <th>PRECIO</th>
                        <th>CANTIDAD</th>
                        <th>SUB TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($Solicituds as $solicitud)
                    <tr>
                        <td>{{ $solicitud->id}}</td>
                        <td>{{ $solicitud->descripcion}}</td>
                        <td>{{ $solicitud->precio}}</td>
                        <td>{{ $solicitud->cantidad}}</td>
                        <td>{{ $solicitud->precio * $solicitud->cantidad}}</td>
                    </tr>
                    @endforeach 
                </tbody>
                <tfoot>
                    <th>TOTAL</th>
                    <th></th>
                    <th></th>
                    <th></th>
                     @foreach ($Total as $total)
                <th><h4>{{$total->total}}</h4></th>
                  @endforeach
                </tfoot>
            </table>
                <div>
                    <a href={{ url('/solicitud') }} class="btn btn-primary">Volver</a>        
                </div>           
         </div>         
         @section('footer')
         <div class="alert alert-primary" role="alert">
             -   
         <div class="float-right d-none d-sm-inline-block">

           </div>
         </div>
         @endsection
@endsection