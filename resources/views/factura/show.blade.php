@extends('layouts.layout')

@section('title','Listado detalles')
    
@section('content')
@empty ($detalles)
    <div class="alert alert-warning">
        Lista de detalle
    </div>
@else
<div id="page_pdf">
	<table id="factura_head">
		<tr>
			<td class="logo_factura">
				<div>
					<img src="{{asset('/images/logo/logo_network.jpg')}}" width="200">
				</div>
			</td>
			<td class="info_empresa">
				<div>
					<span class="h2"><strong>NETWORK VISION</strong></span>
					<p>Canal Isuto entre 3er y 4to anillo calle san antonio, Santa Cruz de la Sierra, Bolivia                   </p>
                    <p>Teléfono: +(591) 6789-1234</p>
					<p>Email: networkventas@gmail.com</p>
				</div>
			</td>
			<td class="info_factura">
				<div class="round">
					<span class="h3"><strong>Factura</strong></span>
					<p>No. Factura: <strong>0000{{$facturas->id}}</strong></p>
					<p>Fecha: {{$facturas->fecha}}</p>
					<p>Vendedor: {{Auth::user()->name}}</p>
				</div>
			</td>
		</tr>
	</table>
	<table id="factura_cliente">
		<tr>
			<td class="info_cliente">
				<div class="round">
					<span class="h3"></span>
					<table class="datos_cliente">
						<tr>
							<td><label>Nit:</label><p>{{$facturas->nit}}</p></td>
						</tr>
						<tr>
							<td><label>Razon Social:</label> <p>{{$facturas->razonsocial}}</p></td>
						</tr>
					</table>
				</div>
			</td>

		</tr>
	</table>
<div class="table-responsive">
    <table class="table table-striped" >
        <thead style="background-color:#A9D0F5">
            <tr>
                <th>ITEM</th>
                <th>CANTIDAD</th>
                <th>P.U.</th>
                <th>SUB TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalles as $detalle)
            <tr>
                <td>{{ $detalle->nombre}}</td>
                <td>{{ $detalle->cantidad}}</td>
                <td>{{ $detalle->precio}}</td>
                <td>{{ $detalle->precio * $detalle->cantidad}}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <th>TOTAL</th>
            <th></th>
            <th></th>
            <th><h4>Bs.{{$facturas->total}}</h4></th>
            <td>

            </td>
        </tfoot>
        
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
