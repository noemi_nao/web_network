@extends('layouts.layout')

@section('title','Listado presupuestos')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">presupuesto</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
@empty ($presupuestos)
    <div class="alert alert-warning">
        Lista de presupuesto
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>descripcion</th>
                <th>NOMBRE</th>
                <th>fechasolicitud</th>
                <th>estado</th>
                <th>total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($presupuestos as $presupuesto)
            <tr>
                <td>{{ $presupuesto->id}}</td>
                <td>{{ $presupuesto->descripcion}}</td>
                <td>{{ $presupuesto->nombre}}</td>
                <td>{{ $presupuesto->fechasolicitud}}</td>
                <td>{{ $presupuesto->estado}}</td>
                <td>{{ $presupuesto->total}}</td>

                <td>
                    <a class="btn btn-success" href="{{ route('factura.create',$presupuesto->id)}}">Create</a>
                        <a class="btn btn-primary" 
                        href="{{ route('presupuesto.edit',$presupuesto->id)}}">Edit</a>
                        @if (Auth::user()->tipo_useriario_id==1) 
                        <form method="POST" class="d-inline" action="{{ route('presupuesto.destroy',$presupuesto->id )}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        @endif
                        </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
