@extends('layouts.app')

@section('title','Modificacion cliente')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<h1>Modificacion de clientes</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

    <form class="form-group" action="/cliente/{{$cliente->id}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
       <div class="form-group">
            <label for="">Documento</label>
            <input type="text" name="documento" value = "{{$cliente->documento}}" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" name="nombre" value = "{{$cliente->nombre}}" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Apellidos</label>
            <input type="text" name="apellidos" value = "{{$cliente->apellidos}}" class="form-control">
       </div>
       <div class="control-group">
          <label class="control-label">FECHA DE NACIMIENTO</label>
          <div class="controls">
              <input name="fechanac" type="date" value = "{{$cliente->fechanac}}" class="span11" placeholder="Fecha Nacimiento"/>
          </div>
      </div>
      <div class="form-group">
          <label for="">SEXO</label>
         <select name="sexo" id="inputSexo_id" class="form-control">
           <option value="F">FEMENINO</option>
           <option value="M">MASCULINO</option>
         </select>
     </div>
       <div class="form-group">
            <label for="">telefono</label>
            <input type="text" name="telefono" value = "{{$cliente->telefono}}" class="form-control">
       </div>
       <div class="form-group">
          <label for="">direccion</label>
          <input type="text" name="direccion" value = "{{$cliente->direccion}}" class="form-control">
     </div>
     <div class="form-group">
          <label for="">email</label>
          <input type="text" name="email" value = "{{$cliente->email}}" class="form-control">
     </div>
       <div class="form-group">
            <label for="">Foto</label>
            <input type="file" name="foto" class="form-control">
       </div>
       <button type="submit" class="btn btn-primary">Modificar</button>
       </div>   
    </form>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection