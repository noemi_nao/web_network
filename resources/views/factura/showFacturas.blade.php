@extends('layouts.layout')

@section('title','Listado factura')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Listado de facturas</h1>
</font>
<h1>    
</h1>
@empty ($facturas)
    <div class="alert alert-warning">
        Lista de factura
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>NRO</th>
                <th>CLIENTE</th>
                <th>DESCRIPCION</th>
                <th>TOTAL</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($facturas as $factura)
            <tr>
                <td>{{ $factura->id}}</td>
                <td>{{ $factura->nombre}}</td>
                <td>{{ $factura->descripcion}}</td>
                <td>{{ $factura->total}}</td>
                <td>
                        <a class="btn btn-primary" 
                        href="{{ route('factura.show',$factura->id)}}">Ver Detalle</a>
                        @if (Auth::user()->tipo_useriario_id==1) 
                        <form method="POST" class="d-inline" action="{{ route('factura.destroy',$factura->id )}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        @endif
                        </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
