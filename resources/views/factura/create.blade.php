@extends('layouts.layout')

@section('title','Registro factura')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de Facturas</h1>
</font>
<h1> 
    
</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('factura.store') }}" enctype="multipart/form-data">
         @csrf
          <div class="form-group row" > 
               <label for="idpresupuesto" class="col-md-4 col-form-label text-md-right">{{ __('idpresupuesto') }}</label>
               <div class="col-md-6">
                   <input id="idpresupuesto" type="text" class="form-control @error('idpresupuesto') is-invalid @enderror" name="idpresupuesto" value="{{$presupuesto->id}}" required autocomplete="idpresupuesto" autofocus>
                   @error('idpresupuesto')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                   @enderror
               </div>
           </div>

          <div class="form-group row" > 
             <label for="nit" class="col-md-4 col-form-label text-md-right">{{ __('nit') }}</label>
             <div class="col-md-6">
                 <input id="nit" type="text" class="form-control @error('nit') is-invalid @enderror" name="nit" value="{{ old('nit') }}" required autocomplete="nit" autofocus>
                 @error('nit')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
       
         <div class="form-group row">
             <label for="razonsocial" class="col-md-4 col-form-label text-md-right">{{ __('razonsocial') }}</label>
             <div class="col-md-6">
                 <input id="razonsocial" type="text" class="form-control @error('razonsocial') is-invalid @enderror" name="razonsocial" value="{{ old('razonsocial') }}" required autocomplete="razonsocial">
             @error('razonsocial')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
          <label for="fecha" class="col-md-4 col-form-label text-md-right">{{ __('fecha') }}</label>
          <div class="col-md-6">
               <input name="fecha" type="date" class="span11" placeholder="Fecha Nacimiento" required/>
          </div>
           </div>

     
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
