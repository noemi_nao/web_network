@extends('layouts.layout')

@section('title','Registro Almacen')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<h1>Registro de Almacen</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('almacen.store') }}" enctype="multipart/form-data">
         @csrf
          <h1>Registro de Almacen</h1>       

         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
             <div class="col-md-6">
                 <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion">
             @error('descripcion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
        
             <div class="form-group row">
             <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
             <div class="col-md-6">
                 <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion">
             @error('direccion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
    <div class="form-row">
          <label for="regional_id" class="col-md-4 col-form-label text-md-right">{{ __('Regional') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="regional_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Regionals->all() as $regional)
                                   <option value="{{$regional->id}}">{{$regional->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('regional_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

           <div class="form-row">
          <label for="tipoalmacen_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo Almacen') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="tipoalmacen_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($TipoAlmacens->all() as $tipoalmacen)
                                   <option value="{{$tipoalmacen->id}}">{{$tipoalmacen->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('tipoalmacen_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
         <!--  datos añadidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
