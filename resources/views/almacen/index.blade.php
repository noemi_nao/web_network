@extends('layouts.layout')

@section('title','Listado Almacens')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Almacens</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
<a class="btn btn-success" href="{{ route('almacen.create')}}">Create</a>
@empty ($Almacens)
    <div class="alert alert-warning">
        No existen Almacens Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>id</th>
                <th>Descripcion</th>
                <th>Estado</th>     
                <th>Direccion</th>    
            </tr>
        </thead>
        <tbody>
            @foreach ($Almacens as $almacen)
            <tr>
                <td>{{$almacen->id}}</td>
                <td>{{$almacen->descripcion}}</td>
                <td>{{$almacen->estado}}</td>
                <td>{{$almacen->direccion}}</td>
                <td>
              <a href="{{ route('almacen.show',$almacen->id)}}" class="btn btn-secondary">Ver mas..</a>  
           
              <a class="btn btn-primary" 
                href="{{ route('almacen.edit',$almacen->id)}}">Edit</a>
               <form method="POST" class="d-inline" action="{{ route('almacen.destroy',$almacen->id )}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
      
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection