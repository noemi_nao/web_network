@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de presupuestos</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body">
     <form method="POST" action="{{ route('presupuesto.store') }}" enctype="multipart/form-data">
         @csrf 
      
         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('descripcion') }}</label>
             <div class="col-md-6">
                 <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion">
             @error('descripcion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>

            <div class="form-group row">
            <label for="fecha" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Solicitud') }}</label>

            <div class="col-md-6">
                <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha" autofocus>
                @error('fecha')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">         
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
