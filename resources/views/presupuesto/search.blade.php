@extends('layouts.layout')
@section('title','Registro de Proveedores')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<h1>Registro de Proveedores</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="container">
     <div class="row">
    <form class="form-group" action="{{url('/proveedor')}}" method="post" enctype="multipart/form-data">
   
        @csrf
       <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" name="nombre" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Direccion</label>
            <input type="text" name="direccion" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Telefono</label>
            <input type="text" name="telefono" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" class="form-control">
       </div>
       <div class="form-group">
            <label for="">Logo</label>
            <input type="file" name="logo" class="form-control">
       </div>
       <button type="submit" class="btn btn-primary">Guardar</button>
       </div>   
    </form>
</div> 
</div> 
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@section('script')
@endsection
@endsection
@endsection