@extends('layouts.layout')

@section('title','presupuesto')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Detalles Presupuesto</h1>
</font>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>
         
     @endif
           <div class="text-center"> 
            <!--  <h5 class="card-title">{{--$presupuesto->nombre--}} </h5> -->
   
            <table class="table table-striped" >
                <thead class="thrad-light">
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>PRECIO</th>
                        <th>CANTIDAD</th>
                        <th>SUB TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($presupuestos as $presupuesto)
                    <tr>
                        <td>{{ $presupuesto->id}}</td>
                        <td>{{ $presupuesto->nombre}}</td>
                        <td>{{ $presupuesto->precio}}</td>
                        <td>{{ $presupuesto->cantidad}}</td>
                        <td>{{ $presupuesto->precio * $presupuesto->cantidad}}</td>
                    </tr>
                    @endforeach 
                </tbody>
                <tfoot>
                    <th>TOTAL</th>
                    <th></th>
                    <th></th>
                    <th></th>
                <th><h4>{{$total->total}}</h4></th>
                </tfoot>
            </table>
                <div>
                    <a href={{ url('/home') }} class="btn btn-primary">Volver</a>        
                </div>           
         </div>         
         @section('footer')
         <div class="alert alert-primary" role="alert">
             -   
         <div class="float-right d-none d-sm-inline-block">
             <b>Visitas:</b>{{$menus->vistas}}
           </div>
         </div>
         @endsection
@endsection