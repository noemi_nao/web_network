@extends('layouts.layout')

@section('title','Modificacion item')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<h1>Modificacion de items</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif


    <form class="form-group" action="{{ route('item.update', $item->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
    <div class="card-body">
     <form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <h1>Registro de Items</h1>     
          <div class="form-group row" > 
             <label for="codigo" class="col-md-4 col-form-label text-md-right">{{ __('Codigo') }}</label>
             <div class="col-md-6">
                 <input id="codigo" type="text" value = "{{$item->codigo}}" class="form-control @error('codigo') is-invalid @enderror" name="codigo" value="{{ old('codigo') }}" required autocomplete="codigo" autofocus>
                 @error('codigo')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
       
         <div class="form-group row">
             <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
 
             <div class="col-md-6">
                 <input id="nombre" type="text" value = "{{$item->nombre}}" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre">
             @error('nombre')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
 
             <div class="col-md-6">
                 <input id="descripcion" type="text" value = "{{$item->descripcion}}" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>
 
                 @error('descripcion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="precio" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>
 
             <div class="col-md-6">
                 <input id="precio" type="text"  value = "{{$item->precio}}" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio') }}" required autocomplete="precio">
 
                 @error('precio')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>

         <div class="form-row">
          <label for="tipo_item_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Item') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="tipo_item_id" required>
                              <option value="selected">Selecionar...</option>
                              @foreach ($tipoitems->all() as $tipoitem)
                                   <option value="{{$tipoitem->id}}">{{$tipoitem->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('tipo_item_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
          <div class="form-row">
            <label for="foto_id" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>
            <div class="col-md-6">
             <input type="file" name="foto" class="form-control">
            </div>
        </div>
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
                 <button type="submit" class="btn btn-primary">
                     {{ __('Editar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{ url('/home') }}">Cancelar</a>
             </div>
         </div>
        
     </form>
 </div>


@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection