@extends('layouts.layout')

@section('title','Registro trabajo')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de trabajos</h1>
</font>
<h1>
    
</h1>
<div class="card-body">
     <form method="POST" action="{{ route('trabajo.store') }}" enctype="multipart/form-data">
         @csrf 
         <div class="form-group row" > 
            <label for="idpresupuesto" class="col-md-4 col-form-label text-md-right">{{ __('# presupuesto') }}</label>
            <div class="col-md-6">
                <input id="idpresupuesto" type="text" class="form-control @error('idpresupuesto') is-invalid @enderror" name="idpresupuesto" value="{{$presupuestos->id}}" required autocomplete="idpresupuesto" autofocus>
                @error('idpresupuesto')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
      
          <div class="form-group row" > 
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('descripcion') }}</label>
             <div class="col-md-6">
                 <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{$presupuestos->descripcion}}" required autocomplete="descripcion" autofocus>
                 @error('descripcion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
       
         <div class="form-group row">
             <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('direccion') }}</label>
             <div class="col-md-6">
                 <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion">
             @error('direccion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         
         <div class="form-group row">
            <label for="fechaejecucion" class="col-md-4 col-form-label text-md-right">{{ __('fechae jecucion') }}</label>
            <div class="col-md-6">
                 <input name="fechaejecucion" type="date" class="span11" placeholder="fechaejecucion Nacimiento" required/>
            </div>
             </div>
             <div class="form-group row">
                <label for="fechaconclusion" class="col-md-4 col-form-label text-md-right">{{ __('fecha conclusion') }}</label>
                <div class="col-md-6">
                     <input name="fechaconclusion" type="date" class="span11" placeholder="fechaconclusion Nacimiento" required/>
                </div>
                 </div>
      
  
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
