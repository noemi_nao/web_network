@extends('layouts.layout')

@section('title','item')
    
@section('content')
<h1>Datos Item</h1>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>     
     @endif
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images/items')}}/{{$item->foto}}" alt="">
           <div class="text-center"> 
           <h5 class="card-title">{{$item->nombre}} </h5>             
                  <p> {{$item->codigo}}</p>
                  <p> {{$item->nombre}}</p>
                  <p> {{$item->descipcion}}</p>
                  <p> {{$item->precio}}</p>
                  <p> {{$item->stock}}</p>
                  <a href={{ url('/home') }} class="btn btn-primary">Volver</a>   
             </form>
         </div> 
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection