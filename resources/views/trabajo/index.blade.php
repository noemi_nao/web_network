@extends('layouts.layout')

@section('title','Listado presupuestos')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Presupuestos</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
@empty ($presupuestos)
    <div class="alert alert-warning">
        No existen presupuestos Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>DESCRIPCION</th>
                <th>ESTADO</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($presupuestos as $presupuesto)
            <tr>
                <td>{{ $presupuesto->id}}</td>
                <td>{{ $presupuesto->descripcion}}</td>
                <td>{{ $presupuesto->estado}}</td>
                <td>{{ $presupuesto->total}}</td>
                <td> 
                    <a class="btn btn-success" href="{{ route('trabajo.create',$presupuesto->id)}}">Create</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
@endsection