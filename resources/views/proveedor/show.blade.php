@extends('layouts.layout')

@section('title','Proveedor')
    
@section('content')

     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>
         
     @endif
     <font size=32 style="color:#132735" face="Segoe UI">
        <h1 align="center">Datos Proveedor</h1>
    </font>
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images/proveedores')}}/{{$proveedor->logo}}" alt="">
           <div class="text-center"> 
            <!--  <h5 class="card-title">{{--$proveedor->nombre--}} </h5> -->
            <h1>{{$proveedor->nombre}}</h1>            
                  <p> {{$proveedor->direccion}}</p>
                  <p> {{$proveedor->telefono}}</p>
                  <p> {{$proveedor->email}}</p>
                  <a href={{ url('/home') }} class="btn btn-primary">Volver</a>                  
         </div>         
         @section('footer')
         <div class="alert alert-primary" role="alert">
             -   
         <div class="float-right d-none d-sm-inline-block">
             <b>Visitas:</b>{{$menus->vistas}}
           </div>
         </div>
         @endsection
@endsection