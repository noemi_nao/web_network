@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de Proveedores</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body">
     <form method="POST" action="{{ route('proveedor.store') }}" enctype="multipart/form-data">
         @csrf     
      
         <div class="form-group row">
             <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
             <div class="col-md-6">
                 <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre">
             @error('nombre')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
              <div class="form-group row">
             <label for="nit" class="col-md-4 col-form-label text-md-right">{{ __('Nit') }}</label>
 
             <div class="col-md-6">
                 <input id="nit" type="text" class="form-control @error('nit') is-invalid @enderror" name="nit" value="{{ old('nit') }}" required autocomplete="nit" autofocus>
 
                 @error('nit')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
   <div class="form-group row" > 
          <label for="correo" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>
          <div class="col-md-6">
              <input id="correo" type="text" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" required autocomplete="correo" autofocus>
              @error('correo')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
         </div>
         <div class="form-group row">
             <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
 
             <div class="col-md-6">
                 <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>
 
                 @error('direccion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>
 
             <div class="col-md-6">
                 <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono">
 
                 @error('telefono')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
      
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">         
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
