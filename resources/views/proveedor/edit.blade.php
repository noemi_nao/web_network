@extends('layouts.layout')

@section('title','Modificacion Proveedor')

@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<h1>Modificacion de Proveedores</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<form class="form-group" action="{{ route('proveedor.update', $proveedor->id) }}" method="post" enctype="multipart/form-data">
     @method('PUT')
     @csrf
 <div class="card-body">
  <form method="POST" action="{{ route('proveedor.store') }}" enctype="multipart/form-data">
       @csrf
       @method('PUT')
       <h1>Modificacion de Proveedores</h1> 
       <div class="form-group row">
          <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

          <div class="col-md-6">
              <input id="nombre" type="text" value = "{{$proveedor->nombre}}" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre">
          @error('nombre')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          </div>
      </div>    
       <div class="form-group row" > 
          <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
          <div class="col-md-6">
              <input id="direccion" type="text" value = "{{$proveedor->direccion}}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>
              @error('direccion')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
    
    
      <div class="form-group row">
          <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>

          <div class="col-md-6">
              <input id="telefono" type="text" value = "{{$proveedor->telefono}}" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" autofocus>

              @error('telefono')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row">
          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Tmail') }}</label>

          <div class="col-md-6">
              <input id="email" type="text"  value = "{{$proveedor->email}}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
       <div class="form-row">
         <label for="logo_id" class="col-md-4 col-form-label text-md-right">{{ __('Logo') }}</label>
         <div class="col-md-6">
          <input type="file" name="logo" class="form-control">
         </div>
     </div>
      <!--  datos aÃ±adidos hasta aqui-->
      <div class="form-group row mb-3">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Editar') }}
              </button>
              <a class="btn btn-danger col-md-2 offset-md-4" 
              href="{{ url('/home') }}">Cancelar</a>
          </div>
      </div>
     
  </form>
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection