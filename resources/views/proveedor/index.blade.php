@extends('layouts.layout')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Proveedores</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div> 
    </div>
  </form>    
</h1>
<a class="btn btn-success" href="{{ route('proveedor.create')}}">Create</a>

@empty ($proveedores)
    <div class="alert alert-warning">
         No existen Proveedores Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCION</th>
                <th>TELEFONO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($proveedores as $proveedor)
            <tr>
                <td>{{ $proveedor->id}}</td>
                <td>{{ $proveedor->nombre}}</td>
                <td>{{ $proveedor->direccion}}</td>
                  <td>{{ $proveedor->telefono}}</td>
                <td>      
                    <a href="{{ route('proveedor.show',$proveedor->id)}}" class="btn btn-secondary">Ver mas..</a>  
                    <a class="btn btn-primary" 
                      href="{{ route('proveedor.edit',$proveedor->id)}}">Edit</a>
                     <form method="POST" class="d-inline" action="{{ route('proveedor.destroy',$proveedor->id )}}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
@endsection
