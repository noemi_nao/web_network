@extends('layouts.layout')

@section('title','item')
    
@section('content')

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Datos Item</h1>
</font>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>     
     @endif
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images/items')}}/{{$item->foto}}" alt="">
           <div class="text-center"> 
           <h5 class="card-title"></h5> 
                  <p><strong>{{$item->nombre}}</strong></p>            
                  <p>Codigo : {{$item->codigo}}</p>
                  <p>Nombre :  {{$item->nombre}}</p>
                  <p>Descripcion : {{$item->descipcion}}</p>
                  <p>Precio Bs.: {{$item->precio}}</p>
                  <p>Stock : {{$item->stock}}</p>
                  <a href={{ url('/home') }} class="btn btn-primary">Volver</a>   
             </form>
         </div> 
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection