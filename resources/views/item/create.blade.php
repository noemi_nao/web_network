@extends('layouts.layout')

@section('title','Registro item')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de Items</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data">
         @csrf    
       

         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
 
             <div class="col-md-6">
                 <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>
 
                 @error('descripcion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>

              <div class="form-group row">
             <label for="observacion" class="col-md-4 col-form-label text-md-right">{{ __('Observacion') }}</label>
             <div class="col-md-6">
                 <input id="observacion" type="text" class="form-control @error('observacion') is-invalid @enderror" name="observacion" value="{{ old('observacion') }}" required autocomplete="observacion">
             @error('observacion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>

         <div class="form-group row">
             <label for="umbral" class="col-md-4 col-form-label text-md-right">{{ __('Umbral') }}</label>
 
             <div class="col-md-6">
                 <input id="umbral" type="text" class="form-control @error('umbral') is-invalid @enderror" name="umbral" value="{{ old('umbral') }}" required autocomplete="umbral">
 
                 @error('umbral')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>

    <div class="form-row">
          <label for="proveedor_id" class="col-md-4 col-form-label text-md-right">{{ __('Proveedor') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="proveedor_id" required>
                              <option value="selected">Selecionar...</option>
                              @foreach ($proveedors->all() as $proveedor)
                                   <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
                              @endforeach          
                    </select>
                    @error('proveedor_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

         <div class="form-row">
          <label for="tipoitem_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Item') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="tipoitem_id" required>
                              <option value="selected">Selecionar...</option>
                              @foreach ($tipoitems->all() as $tipoitem)
                                   <option value="{{$tipoitem->id}}">{{$tipoitem->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('tipoitem_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

    <div class="form-row">
          <label for="marca_id" class="col-md-4 col-form-label text-md-right">{{ __('Marca') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="marca_id" required>
                              <option value="selected">Selecionar...</option>
                              @foreach ($marcas->all() as $marca)
                                   <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('marca_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 


    <div class="form-row">
          <label for="medida_id" class="col-md-4 col-form-label text-md-right">{{ __('Medida') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="medida_id" required>
                              <option value="selected">Selecionar...</option>
                              @foreach ($medidas->all() as $medida)
                                   <option value="{{$medida->id}}">{{$medida->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('medida_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

         <div class="form-row">
            <label for="imagen_id" class="col-md-4 col-form-label text-md-right">{{ __('Imagen') }}</label>
            <div class="col-md-6">
             <input type="file" name="imagen" class="form-control">
            </div>
        </div>
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
