@extends('layouts.layout')
@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['ITEM', 'PORCENTAJE VENTAS'],
     @foreach($vistas as $vista)
     ['{{$vista->descripcion}}',{{$vista->porcentaje}}],
     @endforeach
    ]);

    var options = {
      title: 'Porcentaje de Ventas'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
</script>
<div id="piechart" style="width: 900px; height: 500px;"></div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection