@extends('layouts.layout')

@section('title','Estadisticas')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Listado de Estadisticas</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>

<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>#</th>
                <th>DESCRIPCION</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Porcentaje de Ventas</td>
                <td>
                    <a href="{{route('estadistica.1')}}" class="btn btn-secondary">Ver mas..</a>                        
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Ventas por Vendedor</td>
                <td>
                    <a href="{{route('estadistica.2')}}" class="btn btn-secondary">Ver mas..</a>                        
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Estadisticas de Accesos del Sitio</td>
                <td>
                    <a href="{{route('estadistica.3')}}" class="btn btn-secondary">Ver mas..</a>                        
                </td>
            </tr>

        </tbody>
    </table>
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
