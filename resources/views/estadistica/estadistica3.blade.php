@extends('layouts.layout')
@section('content')

<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
<script>
   google.load("visualization", "1", {packages:["corechart"]});
   google.setOnLoadCallback(dibujarGrafico);
   function dibujarGrafico() {
     // Tabla de datos: valores y etiquetas de la gráfica
       var data = google.visualization.arrayToDataTable([
      ['Menu', '#vistas'],
     @foreach($vistas as $vista)
     ['{{$vista->menu}}',{{$vista->vistas}}],
     @endforeach
    ]);
     var options = {
       
       title: 'Estadisticas de Accesos del Sitio'
     }
     // Dibujar el gráfico
     new google.visualization.ColumnChart( 
     //ColumnChart sería el tipo de gráfico a dibujar
       document.getElementById('GraficoGoogleChart-ejemplo-1')
     ).draw(data, options);
   }
 </script> 
<body>
 
<div id="GraficoGoogleChart-ejemplo-1" style="width: 800px; height: 600px">
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
