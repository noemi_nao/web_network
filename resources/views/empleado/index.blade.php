@extends('layouts.layout')

@section('title','Listado Empleados')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Empleados</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
<a class="btn btn-success" href="{{ route('empleado.create')}}">Create</a>
@empty ($Empleados)
    <div class="alert alert-warning">
        Lista de empleado
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>CODIGO EMPLEADO</th>
                <th>DOCUMENTO</th>
                <th>NOMBRE</th>
                <th>APELLIDO PARTERNO</th>
                <th>APELLIDO MATERNO</th>
                <th>NACIMIENTO</th>
                <th>SEXO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Empleados as $empleado)
            <tr>
                <td>{{ $empleado->id}}</td>
                <td>{{ $empleado->documento}}</td>
                <td>{{ $empleado->nombre}}</td>
                <td>{{ $empleado->apellidop}}</td>
                <td>{{ $empleado->apellidom}}</td>
                <td>{{ $empleado->fechanac}}</td>
                <td>{{ $empleado->sexo}}</td>

                <td>
                        <a class="btn btn-primary" 
                        href="{{ route('empleado.edit',$empleado->id)}}">Edit</a>
                        <form method="POST" class="d-inline" action="{{ route('empleado.destroy',$empleado->id )}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
  </div>
</div>
@endsection
