@extends('layouts.app')

@section('title','empleado')
    
@section('content')
<h1>Datos empleado</h1>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>
         
     @endif
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images')}}/{{$empleado->foto}}" alt="">
           <div class="text-center"> 
           <h5 class="card-title">{{$empleado->nombre}} </h5>             
                  <p> {{$empleado->direccion}}</p>
                  <p> {{$empleado->telefono}}</p>
                  <p> {{$empleado->email}}</p>
                  <a href="/empleado/{{$empleado->id}}/edit" class="btn btn-primary">Editar</a>   
                  <form class="form-group" action="/empleado/{{$empleado->id}}" method="post" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Eliminar</button>   
             </form>
         </div> 
           

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection