@extends('layouts.app')

@section('title','cliente')
    
@section('content')
<h1>Datos cliente</h1>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>
         
     @endif
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images')}}/{{$cliente->foto}}" alt="">
           <div class="text-center"> 
           <h5 class="card-title">{{$cliente->nombre}} </h5>              
                  <p> {{$cliente->direccion}}</p>
                  <p> {{$cliente->telefono}}</p>
                  <p> {{$cliente->email}}</p>
                  <a href="/cliente/{{$cliente->id}}/edit" class="btn btn-primary">Editar</a>   
                  <form class="form-group" action="/cliente/{{$cliente->id}}" method="post" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Eliminar</button>   
             </form>
         </div> 
           

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
