@extends('layouts.layout')

@section('title','Listado Clientes')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Clientes</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
<a class="btn btn-success" href="{{ route('cliente.create')}}">Create</a>
@empty ($Clientes)
    <div class="alert alert-warning">
        Lista de clientes
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>CODIGO CLIENTE</th>
                <th>DOCUMENTO</th>
                <th>NOMBRE</th>
                <th>APELLIDO PARTERNO</th>
                <th>APELLIDO MATERNO</th>
                <th>NACIMIENTO</th>
                <th>SEXO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Clientes as $cliente)
            <tr>
                <td>{{ $cliente->id}}</td>
                <td>{{ $cliente->documento}}</td>
                <td>{{ $cliente->nombre}}</td>
                <td>{{ $cliente->apellidop}}</td>
                <td>{{ $cliente->apellidom}}</td>
                <td>{{ $cliente->fechanac}}</td>
                <td>{{ $cliente->sexo}}</td>

                <td>
                        <a class="btn btn-primary" 
                        href="{{ route('cliente.edit',$cliente->id)}}">Edit</a>
                        <form method="POST" class="d-inline" action="{{ route('cliente.destroy',$cliente->id )}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
  </div>
</div>
@endsection
