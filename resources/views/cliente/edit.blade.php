@extends('layouts.layout')

@section('title','Modificacion cliente')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Modificacion Cliente</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<form class="form-group" action="{{ route('cliente.update', $Cliente->id) }}" method="post" enctype="multipart/form-data">
     @method('PUT')
     @csrf
 <div class="card-body">
  <form method="POST" action="{{ route('cliente.store') }}" enctype="multipart/form-data">
       @csrf
       @method('PUT')
      
       <div class="form-group row">
          <label for="documento" class="col-md-4 col-form-label text-md-right">{{ __('Documento') }}</label>

          <div class="col-md-6">
              <input id="documento" type="text"  class="form-control @error('documento') is-invalid @enderror" name="documento" value="{{$Cliente->documento}}" required autocomplete="documento">
          @error('documento')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          </div>
      </div>    
       <div class="form-group row" > 
          <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
          <div class="col-md-6">
              <input id="nombre" type="text" value = "{{$Cliente->nombre}}" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
              @error('nombre')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
    
    
      <div class="form-group row">
          <label for="apellidos" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

          <div class="col-md-6">
              <input id="apellidos" type="text" value = "{{$Cliente->apellidos}}" class="form-control @error('apellidos') is-invalid @enderror" name="apellidos" value="{{ old('apellidos') }}" required autocomplete="apellidos" autofocus>

              @error('apellidos')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row">
          <label for="fechanac" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de nacimiento') }}</label>
          <div class="col-md-6">
              <input id="fechanac" type="date" class="form-control @error('fechanac') is-invalid @enderror" name="fechanac" value="{{ old('fechanac') }}" required autocomplete="fechanac" autofocus>
              @error('fechanac')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
    
      <div class="form-group row">
          <label for="" class="col-md-4 col-form-label text-md-right">Sexo</label>
          <div class="col-md-6">
         <select name="sexo" id="inputSexo_id" class="form-control">
           <option value="F">FEMENINO</option>
           <option value="M">MASCULINO</option>
         </select>
     </div>  </div>

     <div class="form-group row">
          <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>

          <div class="col-md-6">
              <input id="telefono" type="text"  value = "{{$Cliente->telefono}}" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono">

              @error('telefono')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>


      <div class="form-group row">
          <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

          <div class="col-md-6">
              <input id="direccion" type="text"  value = "{{$Cliente->direccion}}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion">

              @error('direccion')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>


      <div class="form-group row">
          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

          <div class="col-md-6">
              <input id="email" type="text"  value = "{{$Cliente->email}}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <!--  datos aÃ±adidos hasta aqui-->
      <div class="form-group row mb-3">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Editar') }}
              </button>
              <a class="btn btn-danger col-md-2 offset-md-4" 
              href="{{ url('/home') }}">Cancelar</a>
          </div>
      </div>
     
  </form>
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$Menus->vistas}}
  </div>
</div>
@endsection
