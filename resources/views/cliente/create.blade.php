@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de Clientes</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body">
     <form method="POST" action="{{ route('cliente.store') }}" enctype="multipart/form-data">
         @csrf     
        <div class="form-group row">
             <label for="documento" class="col-md-4 col-form-label text-md-right">{{ __('Documento') }}</label>
             <div class="col-md-6">
                 <input id="documento" type="text" class="form-control @error('documento') is-invalid @enderror" name="documento" value="{{ old('documento') }}" required autocomplete="documento">
             @error('documento')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
             <div class="col-md-6">
                 <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre">
             @error('nombre')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="apellidoP" class="col-md-4 col-form-label text-md-right">{{ __('Apellido Paterno') }}</label>
             <div class="col-md-6">
                 <input id="apellidoP" type="text" class="form-control @error('apellidoP') is-invalid @enderror" name="apellidoP" value="{{ old('apellidoP') }}" required autocomplete="apellidoP">
             @error('apellidoP')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="apellidoM" class="col-md-4 col-form-label text-md-right">{{ __('Apellido Materno') }}</label>
             <div class="col-md-6">
                 <input id="apellidoM" type="text" class="form-control @error('apellidoM') is-invalid @enderror" name="apellidoM" value="{{ old('apellidoM') }}" required autocomplete="apellidoM">
             @error('apellidoM')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
     <div class="form-group row">
            <label for="fechanac" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Nacimiento') }}</label>
            <div class="col-md-6">
                <input id="fechanac" type="date" class="form-control @error('fechanac') is-invalid @enderror" name="fechanac" value="{{ old('fechanac') }}" required autocomplete="fechanac" autofocus>
                @error('fechanac')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

       <div class="form-row">
          <label for="sexo" class="col-md-4 col-form-label text-md-right">{{ __('Sexo') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="sexo" required>
                              <option value="selected">Selecionar...</option>
                              <option value="F">FEMENINO</option>
                              <option value="M">MASCULINO</option>       
                    </select>
                    @error('sexo')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div>
   <div class="form-group row">
             <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>
             <div class="col-md-6">
                 <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono"> 
                 @error('telefono')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
             <div class="col-md-6">
                 <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>
                 @error('direccion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>

         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">         
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
  
  </div>
</div>
@endsection
