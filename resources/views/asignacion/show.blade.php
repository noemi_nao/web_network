@extends('layouts.layout')

@section('title','funcion')
    
@section('content')
<h1>Datos Funcion</h1>
     @if (session('status'))
      <div class="alert alert-success">
          {{session('status')}}
      </div>     
     @endif
          <img style="height: 200px; width: 200px; margin:20px;" class="card-img-top mx-auto d-block"  src="{{asset('/images/funciones/funcion.jpg')}}" alt="">
           <div class="text-center"> 
           <h5 class="card-title"></h5> 
                  <p><strong>{{$funcion->accion}}</strong></p>            
                  <p>Accion : {{$funcion->accion}}</p>
                  <p>Direccion :  {{$funcion->direccion}}</p>

                  <a href={{ url('/home') }} class="btn btn-primary">Volver</a>   
             </form>
         </div> 
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection