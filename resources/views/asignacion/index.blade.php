@extends('layouts.layout')

@section('title','Listado asignaciones')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Listado de asignaciones</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
@if (Auth::user()->tipo_useriario_id==1) 
<a class="btn btn-success" href="{{ route('asignacion.create')}}">Create</a>
@endif
@empty ($asignaciones)
    <div class="alert alert-warning">
        No existen Asignaciones Creadas
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>id</th>
                <th>Tipo usuario</th>
                <th>Accion</th>
                <th>Direccion</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($asignaciones as $asignacion)
            <tr>
                <td>{{$asignacion->id}}</td>
                <td>{{$asignacion->descripcion}}</td>
                <td>{{$asignacion->accion}}</td>
                <td>{{$asignacion->direccion}}</td>
                <td>
                <form method="POST" class="d-inline" action="{{ route('asignacion.destroy',$asignacion->id )}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
      
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection