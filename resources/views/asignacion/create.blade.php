@extends('layouts.layout')

@section('title','Asignacion Funcion')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<h1>Asignacion de Funciones</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('asignacion.store') }}" enctype="multipart/form-data">
         @csrf
              
          <div class="form-row">
            <label for="tipousuario_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Usuario') }}</label>
                 <div class="col-md-6">
                      <select class="custom-select" name="tipousuario_id" required>
                                <option value="selected">Selecionar...</option>
                                @foreach ($tipousuarios->all() as $tipousuario)
                                     <option value="{{$tipousuario->id}}">{{$tipousuario->descripcion}}</option>
                                @endforeach          
                      </select>
                      @error('tipousuario_id')
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                           </span>
                      @enderror
                 </div>
            </div> 
       
            <div class="form-row">
                <label for="funcion_id" class="col-md-4 col-form-label text-md-right">{{ __('Funciones') }}</label>
                     <div class="col-md-6">
                          <select class="custom-select" name="funcion_id" required>
                                    <option value="selected">Selecionar...</option>
                                    @foreach ($funcions->all() as $funcion)
                                         <option value="{{$funcion->id}}">{{$funcion->accion}}</option>
                                    @endforeach          
                          </select>
                          @error('funcion_id')
                               <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                               </span>
                          @enderror
                     </div>
                </div> 
         
        
         
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
