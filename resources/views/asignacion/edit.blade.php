@extends('layouts.layout')

@section('title','Modificacion Funciones')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif 
<h1>Modificacion de Funciones</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif


    <form class="form-group" action="{{ route('funcion.update', $funcion->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
    <div class="card-body">
     <form method="POST" action="{{ route('funcion.store') }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <h1>Registro de Funciones</h1>     
          <div class="form-group row" > 
             <label for="accion" class="col-md-4 col-form-label text-md-right">{{ __('Accion') }}</label>
             <div class="col-md-6">
                 <input id="accion" type="text" value = "{{$funcion->accion}}" class="form-control @error('accion') is-invalid @enderror" name="accion" value="{{ old('accion') }}" required autocomplete="accion" autofocus>
                 @error('accion')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                 @enderror
             </div>
         </div>
       
         <div class="form-group row">
             <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
 
             <div class="col-md-6">
                 <input id="direccion" type="text" value = "{{$funcion->direccion}}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion">
             @error('direccion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
        
         <!--  datos aÃ±adidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
                 <button type="submit" class="btn btn-primary">
                     {{ __('Editar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{ url('/home') }}">Cancelar</a>
             </div>
         </div>
        
     </form>
 </div>


@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection