@extends('layouts.layout')

@section('title','Listado Modulos')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Modulos</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
<a class="btn btn-success" href="{{ route('modulo.create')}}">Create</a>
@empty ($Modulos)
    <div class="alert alert-warning">
        No existen Modulos Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>id</th>
                <th>Descripcion</th>
                <th>Ruta</th>         
            </tr>
        </thead>
        <tbody>
            @foreach ($Modulos as $modulo)
            <tr>
                <td>{{$modulo->id}}</td>
                <td>{{$modulo->descripcion}}</td>
                <td>{{$modulo->ruta}}</td>
             
                <td>
              <a href="{{ route('modulo.show',$modulo->id)}}" class="btn btn-secondary">Ver mas..</a>  
           
              <a class="btn btn-primary" 
                href="{{ route('modulo.edit',$modulo->id)}}">Edit</a>
               <form method="POST" class="d-inline" action="{{ route('modulo.destroy',$modulo->id )}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
      
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection