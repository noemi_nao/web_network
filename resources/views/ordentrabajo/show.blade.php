@extends('layouts.layout')

@section('title','Detalle Orden de Trabajo')
    
@section('content')
<div id="page_pdf">
	<table id="factura_head">
		<tr>
			<td class="logo_factura">
				<div>
					<img src="{{asset('/images/logo/logo_datacom.jpg')}}" width="200">
				</div>
			</td>
			<td class="info_empresa">
				<div>
					<span class="h2"><strong>Orden de Trabajo</strong></span>
                    <p><strong>Tipo Orden:</strong>{{$Tipo[0]->descripcion}}</p>
                    <p><strong>Localidad:</strong> {{$Localidad[0]->descripcion}}</p>
					
                    <p><strong>Teléfono:</strong> +(591) {{$Cliente[0]->telefono}}   +(591) {{$Orden[0]->telefono1}} </p>
				</div>
			</td>
			<td class="info_factura">
				<div class="round">
					<span class="h3"><strong></strong></span>
					<p><strong>No. Orden Trabajo:</strong> <strong>0000{{$Orden[0]->id}}</strong></p>
					<p><strong>Fecha:</strong> {{$Orden[0]->fechaCreacion}}</p>
				</div>
			</td>
		</tr>
	</table>
	<table id="factura_cliente">
		<tr>
			<td class="info_cliente">
				<div class="round">
					<span class="h3"></span>
					<table class="datos_cliente">
						<tr>
							<td><label>Cliente:</label> <p>{{$Cliente[0]->nombre}} {{ $Cliente[0]->apellidop}} {{ $Cliente[0]->apellidom}}</p></td>
						</tr>

                        <tr>
							<td><label>Direccion:</label> <p>{{$Cliente[0]->direccion}} </p></td>
						</tr>
					</table>
				</div>
			</td>

		</tr>
	</table>
<div class="table-responsive">
    <td><label>Ferreteria:</label>
    <table class="table table-striped" >
     
        <thead style="background-color:#A9D0F5">
            <tr>
                <th>ITEM</th>
                <th>CANTIDAD</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Ferreteria as $ferreteria)
            <tr>
                <td>{{ $ferreteria->descripcion}}</td>
                <td>{{ $ferreteria->cantidad}}</td>
         
            </tr>
            @endforeach
        </tbody>        
    </table>
</div>

<div class="table-responsive">
    <td><label>Equipos:</label>
    <table class="table table-striped" >
       
        <thead style="background-color:#A9D0F5">
            <tr>
                <th>ITEM</th>
                <th>SERIE</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Equipo as $equipo)
            <tr>
                <td>{{ $equipo->descripcion}}</td>
                <td>{{ $equipo->serie}}</td>
         
            </tr>
            @endforeach
        </tbody>        
    </table>
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
