@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de ordentrabajos</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body"> 
     <form method="POST" action="{{ route('ordentrabajo.store') }}" enctype="multipart/form-data">
         @csrf 
        <div class="form-row">
          <label for="cliente_id" class="col-md-4 col-form-label text-md-right">{{ __('Cliente') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="cliente_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Clientes->all() as $cliente)
                                   <option value="{{$cliente->id}}">{{$cliente->nombre}} {{$cliente->apellidoP}} {{$cliente->apellidoM}}</option>
                              @endforeach          
                    </select>
                    @error('cliente_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

           <div class="form-row">
          <label for="tipoorden_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo De Orden') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="tipoorden_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($TipoOrdens->all() as $tipoorden)
                                   <option value="{{$tipoorden->id}}">{{$tipoorden->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('tipoorden_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
 <div class="form-row">
          <label for="localidad_id" class="col-md-4 col-form-label text-md-right">{{ __('localidad') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="localidad_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Localidads->all() as $localidad)
                                   <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('localidad_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
         <div class="form-group row">
             <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>
             <div class="col-md-6">
                 <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono">
             @error('telefono')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>

                  

 
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">         
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
         
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
  
  </div>
</div>
@endsection
