@extends('layouts.layout')
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif

<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de ordentrabajos</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body"> 
     <form method="POST" action="{{ route('ordentrabajo.asignarOT') }}" enctype="multipart/form-data">
         @csrf 

<div class="form-row">
          <label for="almacen_id" class="col-md-4 col-form-label text-md-right">{{ __('cuadrilla') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="almacen_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Cuadrillas->all() as $cuadrilla)
                                   <option value="{{$cuadrilla->id}}">{{$cuadrilla->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('almacen_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

        <div class="table-responsive">
    <h1>Listado de ordentrabajos</h1>
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
              
                <th>Nº OT</th>
                <th>RESULTADO</th>
         <th>SELECT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Ordentrabajos as $ordentrabajo)
            <tr>
                <td>{{ $ordentrabajo->id}}</td>
                <td>{{ $ordentrabajo->resultado}}</td>
                <td>      
                <input  type="checkbox" value="{{ $ordentrabajo->id}}"name="ots[]">
                   
           
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
                  

 
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">         
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
         
     </form>
 </div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
  
  </div>
</div>
@endsection
