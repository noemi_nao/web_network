@extends('layouts.layout')

@section('title','Detalle Orden de Trabajo')
    
@section('content')
<div class="card-body"> 
    <form method="POST" action="{{ route('ordentrabajo.cerrarOT') }}" enctype="multipart/form-data">
        @csrf 
<div id="page_pdf">
	<table id="factura_head">
		<tr>
			<td class="logo_factura">
				<div>
					<img src="{{asset('/images/logo/logo_datacom.jpg')}}" width="200">
				</div>
			</td>
			<td class="info_empresa">
				<div>
					<span class="h2"><strong>Orden de Trabajo</strong></span>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label for="orden" class="col-md-4 col-form-label text-md-right">{{ __('Orden Trabajo:') }}</label>
                            <select name="idorden" id="idorden" class="form-control selectpicker" data-live-search="true">
                                    <option value="{{$Orden[0]->id}}">0000{{$Orden[0]->id}}</option>
                            </select>
                        </div>
                    </div>
                    <p><strong>Tipo Orden:</strong>{{$Tipo[0]->descripcion}}</p>
                    <p><strong>Localidad:</strong> {{$Localidad[0]->descripcion}}</p>
					
                    <p><strong>Teléfono:</strong> +(591) {{$Cliente[0]->telefono}}   +(591) {{$Orden[0]->telefono1}} </p>
				</div>
			</td>
			<td class="info_factura">
				<div class="round">
					<span class="h3"><strong></strong></span>
					
                    
					<p><strong>Fecha:</strong> {{$Orden[0]->fechaCreacion}}</p>
				</div>
			</td>
		</tr>
	</table>
	<table id="factura_cliente">
		<tr>
			<td class="info_cliente">
				<div class="round">
					<span class="h3"></span>
					<table class="datos_cliente">
						<tr>
							<td><label>Cliente:</label> <p>{{$Cliente[0]->nombre}} {{ $Cliente[0]->apellidop}} {{ $Cliente[0]->apellidom}}</p></td>
						</tr>

                        <tr>
							<td><label>Direccion:</label> <p>{{$Cliente[0]->direccion}} </p></td>
						</tr>
					</table>
				</div>
			</td>

		</tr>
	</table>
    <div class="col-md-4">
        <div class="form-group row">
            <label for="item" class="col-md-4 col-form-label text-md-right">{{ __('demora') }}</label>
            <select name="iddemora" id="iddemora" class="form-control selectpicker" data-live-search="true">
                @foreach ($Demora as $demora)
                    <option value="{{ $demora->id }}">{{ $demora->descripcion }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group row">
            <label for="observacion" class="col-md-2 col-form-label text-md-right">{{ __('Observacion') }}</label>
            <input id="observacion" type="text" class="form-control @error('observacion') is-invalid @enderror"
                name="observacion" value="{{ old('observacion') }}" required autocomplete="observacion"
                autofocus>
            @error('observacion')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group row">
            <label for="item" class="col-md-4 col-form-label text-md-right">{{ __('Ferreteria') }}</label>
            <select name="iditem" id="iditem" class="form-control selectpicker" data-live-search="true">
                @foreach ($Ferreteria as $ferreteria)
                    <option value="{{ $ferreteria->id }}">{{ $ferreteria->descripcion }}-{{ $ferreteria->stock }}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="col-md-2">
        <div class="form-group row">
            <label for="idcantidad" class="col-md-2 col-form-label text-md-right">{{ __('Cantidad') }}</label>
            <input id="idcantidad" type="text" class="form-control @error('idcantidad') is-invalid @enderror"
                name="idcantidad" value="{{ old('idcantidad') }}" required autocomplete="idcantidad"
                autofocus>
            @error('idcantidad')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-md-3 offset-md-4">
        <div class="form-group row">
            <button type="button" id="bt_add" class="btn btn-primary">{{ __('Agregar') }}</button>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
            <thead style="background-color:#A9D0F5">
                <th>OPCIONES</th>
                <th>ITEM</th>
                <th>CANTIDAD</th>
                <th>SUB TOTAL</th>
            </thead>
            <tfoot>
                <th>TOTAL</th>
                <th></th>
                <th></th>
                <th>
                    <h4 id="total">0</h4>
                </th>
            </tfoot>
            <tbody>
            </tbody>
        </table>
    </div>

<div class="table-responsive">
        <h1>Listado de ONTS HUAWEI</h1>
        <table class="table table-striped">
            <thead class="thrad-light">
                <tr>
                    <th>DESCRIPCION</th>
                    <th>SERIE</th>
                    <th>SELECT</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Equipos as $equipo)
                    <tr>
                        <td>{{ $equipo->descripcion }}</td>
                        <td>{{ $equipo->serie }}</td>
                        <td>
                            <input type="checkbox" value="{{ $equipo->serie }}" name="huawei[]">

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <h1>Listado de ONTS ZTE</h1>
        <table class="table table-striped">
            <thead class="thrad-light">
                <tr>
                    <th>DESCRIPCION</th>
                    <th>SERIE</th>
                    <th>SELECT</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ztes as $zte)
                    <tr>
                        <td>{{ $zte->descripcion }}</td>
                        <td>{{ $zte->serie }}</td>
                        <td>
                            <input type="checkbox" value="{{ $zte->serie }}" name="zte[]">

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <h1>Listado de IPTVS</h1>
        <table class="table table-striped">
            <thead class="thrad-light">
                <tr>
                    <th>DESCRIPCION</th>
                    <th>SERIE</th>
                    <th>SELECT</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($iptvs as $iptv)
                    <tr>
                        <td>{{ $iptv->descripcion }}</td>
                        <td>{{ $iptv->serie }}</td>
                        <td>
                            <input type="checkbox" value="{{ $iptv->serie }}" name="iptv[]">

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="form-group row mb-3">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Registrar') }}
            </button>
            <a class="btn btn-danger col-md-2 offset-md-4" href="{{ url()->previous() }}">Cancelar</a>
        </div>
    </div>

</div>
@endsection
@section('footer')
    <div class="alert alert-primary" role="alert">

    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                $('#bt_add').click(function() {
                    agregar();
                });
            });
            var cont = 0;
            total = 0;
            subtotal = [];
            $("#guardar").hide();


            function limpiar() {
                $("#cantidad").val("");
            }



            function evaluar() {
                if (total > 0) {
                    $("#guardar").show();
                } else {
                    $("#guardar").hide();
                }
            }


            function agregar(){
                idarticulo=$("#iditem").val();
                articulo=$("#iditem option:selected").text();
                cantidad= $("#idcantidad").val();
                if(iditem!="" && cantidad!="" && cantidad>0 ){
                  subtotal[cont]=cantidad*1;
                  total=total+subtotal[cont];
                  var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td>'+subtotal[cont]+'</td></tr>';
                cont++;
                limpiar();
                $("#total").html(" "+total);
                evaluar();
                $("#detalles").append(fila);
                }
                else{
                    alert("error al ingresar detalle de presupuesto");
                }
                }
              
                function eliminar(index){
                  total=total-subtotal[index]; 
                  $("#total").html("Bs/ " + total);   
                  $("#fila" + index).remove();
                  evaluar();
                }

        </script>
    @endpush
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -
    <div class="float-right d-none d-sm-inline-block">

    </div>
</div>
@endsection
