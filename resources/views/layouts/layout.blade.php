<!DOCTYPE html>
<html lang="en">

<head>
    <title>Datacom S.R.L. </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min') }}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track es el scroll bar derecho*/
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #a7a7a7;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #929292;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        li {
            list-style: none;
        }

        .user-wrapper,
        .message-wrapper {
            border: 1px solid #dddddd;
            overflow-y: auto;
        }

        .user-wrapper {
            height: 500px;
        }

        .user {
            cursor: pointer;
            padding: 5px 0;
            position: relative;
        }

        .user:hover {
            background: #eeeeee;
        }

        .user:last-child {
            margin-bottom: 0;
        }

        .pending {
            position: absolute;
            left: 10px;
            top: 9px;
            background: #66aa26;
            margin: 0;
            border-radius: 50%;
            width: 9px;
            height: 9px;
            line-height: 18px;
            padding-left: 5px;
            color: #ffffff;
            font-size: 12px;
        }

        .media-left {
            margin: 0 10px;
        }

        .media-left img {
            width: 64px;
            border-radius: 64px;
        }

        .media-body p {
            margin: 6px 0;
        }

        .message-wrapper {
            padding: 10px;
            height: 430px;
            background: #eeeeee;
        }

        .messages .message {
            margin-bottom: 15px;
        }

        .messages .message:last-child {
            margin-bottom: 0;
        }

        .received,
        .sent {
            width: 45%;
            padding: 3px 10px;
            border-radius: 10px;
        }

        .received {
            background: #ffffff;
        }

        .sent {
            background: #3bebff;
            float: right;
            text-align: right;
        }

        .message p {
            margin: 5px 0;
        }

        .date {
            color: #777777;
            font-size: 12px;
        }

        .active {
            background: #eeeeee;
        }

        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 15px 0 0 0;
            display: inline-block;
            border-radius: 4px;
            box-sizing: border-box;
            outline: none;
            border: 1px solid #cccccc;
        }

        input[type=text]:focus {
            border: 1px solid #aaaaaa;
        }

    </style>

    <style>
        html {
            font-size: {{ Auth()->user()->font . 'px' }};
        }

    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-{{ Auth()->user()->style }}  navbar-dark ">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('login') }}" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('logout') }}" class="nav-link">Logout</a>
                </li>

            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
                            class="fas fa-th-large"></i></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-{{ Auth()->user()->estilo }} elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="{{ asset('/images/logo/logo_datacom.jpg') }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Datacom S.R.L.</span>
            </a>
            <!--Menu isquierdo-->
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ asset('/images/usuarios') }}/{{ Auth()->user()->imagen }}"
                            class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth()->user()->name }}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                            with font-awesome or any other icon font library -->


                        <?php
                        $modulos = DB::select(
                        'SELECT mo.id,mo.descripcion,COUNT(mo.id) as cantidad FROM modulos AS mo,menus AS me,grupo_menus
                        AS gm
                        WHERE mo.id=me.modulo_id AND me.id=gm.menu_id AND gm.grupo_id=' .
                        Auth()->user()->grupo_id .
                        ' GROUP BY(mo.id) ORDER BY(mo.id)',
                        );

                        $opciones = DB::select(
                        'SELECT mo.id,me.nombre,me.ruta
                        FROM modulos AS mo,menus AS me,grupo_menus AS gm
                        WHERE mo.id=me.modulo_id AND me.id=gm.menu_id AND gm.grupo_id=' .
                        Auth()->user()->grupo_id .
                        '
                        ORDER BY(mo.id)',
                        );
                        $indice = 0;
                        $c = 1;
                        $cantidad = 0;
                        ?>
                        @foreach ($modulos as $modulo)
                            <li class="nav-item has-treeview menu-close">
                                <a href="" class="nav-link active">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        {{ $modulo->descripcion }}
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <?php
                                $cantidad= $modulo->cantidad ;
                                $c=1;
                                ?>
                                @while ($c <= $cantidad)
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="{{$opciones[$indice]->ruta}}" class="nav-link">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>{{ $opciones[$indice]->nombre }}</p>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php
                                    $c=$c+1;
                                    $indice=$indice+1;
                                    ?>
                                @endwhile
                            </li>
                        @endforeach

                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-plus-square"></i>
                                    <p>
                                        Extras
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('usuarios.edit', Auth::user()->id) }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Editar</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- content -->
        <!-- Content Wrapper. Contains page content -->
        <!-- /.content-wrapper -->

        <!-- final content -->
        <!-- Control Sidebar -->

        </aside>
        <div class="content-wrapper">

            @yield('content')
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Opciones <span><i class="ti-close right-side-toggle"></i></span>
                        </div>
                        <div class="r-panel-body">
                            <li class="d-block m-t-30"><b>Color</b></li>
                            <div class="list-group">
                                <a href="{{ 'style/white' }}" class="list-group-item">Blanco</a>
                                <a href="{{ 'style/dark' }}" class="list-group-item">Oscuro</a>
                                <a href="{{ 'style/green' }}" class="list-group-item">Verde</a>
                                <a href="{{ 'style/blue' }}" class="list-group-item">Azul</a>
                                <a href="{{ 'style/orange' }}" class="list-group-item">Naranja</a>
                            </div>
                            <label class="d-block m-t-30"> Tamaño Letra </label>
                            <div class="list-group">
                                <a href="{{ 'font/10' }}" class="list-group-item">10</a>
                                <a href="{{ 'font/12' }}" class="list-group-item">12</a>
                                <a href="{{ 'font/14' }}" class="list-group-item">14</a>
                                <a href="{{ 'font/16' }}" class="list-group-item">16</a>
                                <a href="{{ 'font/18' }}" class="list-group-item">18</a>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        <!-- /.control-sidebar -->


        @yield('footer')
        <!-- Main Footer -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2021 <a href="http://www.datacom.com.bo"></a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0.0
            </div>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
    @stack('scripts')
    <!-- Bootstrap -->
    <script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min') }}"></script>
    <!-- AdminLTE -->
    <script src="{{ asset('adminlte/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('adminlte/js/demo.js') }}"></script>
    <script src="{{ asset('adminlte/js/pages/dashboard3.js') }}"></script>
</body>

</html>
