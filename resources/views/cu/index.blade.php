@extends('layouts.layout')

@section('title','Listado Cus')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Cus</h1>
</font>
<h1> <form class="form-inline " >
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
    
</h1>
<a class="btn btn-success" href="{{ route('cu.create')}}">Create</a>
@empty ($Cus)
    <div class="alert alert-warning">
        No existen Cus Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>id</th>
                <th>Descripcion</th>
                <th>Ruta</th>         
            </tr>
        </thead>
        <tbody>
            @foreach ($Cus as $cu)
            <tr>
                <td>{{$cu->id}}</td>
                <td>{{$cu->descripcion}}</td>
                <td>{{$cu->ruta}}</td>
             
                <td>
              <a href="{{ route('cu.show',$cu->id)}}" class="btn btn-secondary">Ver mas..</a>  
           
              <a class="btn btn-primary" 
                href="{{ route('cu.edit',$cu->id)}}">Edit</a>
               <form method="POST" class="d-inline" action="{{ route('cu.destroy',$cu->id )}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
      
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection