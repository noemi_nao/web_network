@extends('layouts.layout') 
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Registro de Usuarios</h1>
</font>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif
<div class="card-body">
    <form method="POST" action="{{ route('usuarios.store') }}" enctype="multipart/form-data">
        @csrf
    
         
        
      
        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electronico') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>
        <!-- aÃ±adidos -->
       
        
      
          <div class="form-row">
          <label for="grupo_id" class="col-md-4 col-form-label text-md-right">{{ __('grupo') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="grupo_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Grupos->all() as $grupo)
                                   <option value="{{$grupo->id}}">{{$grupo->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('grupo_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
          <div class="form-row">
          <label for="empleado_id" class="col-md-4 col-form-label text-md-right">{{ __('empleado') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="empleado_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Empleados->all() as $empleado)
                                   <option value="{{$empleado->id}}">{{$empleado->nombre }} {{$empleado->apellidoP }} {{$empleado->apellidoM }}</option>
                              @endforeach          
                    </select>
                    @error('empleado_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 

           <div class="form-row">
          <label for="almacen_id" class="col-md-4 col-form-label text-md-right">{{ __('Almacen') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="almacen_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Almacens->all() as $almacen)
                                   <option value="{{$almacen->id}}">{{$almacen->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('almacen_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
          
        <div class="form-row">
            <label for="foto_id" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>
            <div class="col-md-6">
             <input type="file" name="foto" class="form-control">
            </div>
        </div>
     
        <!--  datos aÃ±adidos hasta aqui-->
 
        <div class="form-group row mb-3">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Registrar') }}
                </button>
                <a class="btn btn-danger col-md-2 offset-md-4" 
                href="{{url()->previous()}}">Cancelar</a>
            </div>
        </div>

    </form>
</div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection