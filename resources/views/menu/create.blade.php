@extends('layouts.layout')

@section('title','Registro menu')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<h1>Registro de Menus</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
     @foreach($errors->all() as $error)
       <li>{{$error}}</li>
     @endforeach
     </ul>
     </div>
@endif

<div class="card-body">
     <form method="POST" action="{{ route('menu.store') }}" enctype="multipart/form-data">
         @csrf
          <h1>Registro de Menus</h1>       
          
              <div class="form-group row">
             <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
             <div class="col-md-6">
                 <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre">
             @error('nombre')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
             <div class="form-group row">
             <label for="ruta" class="col-md-4 col-form-label text-md-right">{{ __('Ruta') }}</label>
             <div class="col-md-6">
                 <input id="ruta" type="text" class="form-control @error('ruta') is-invalid @enderror" name="ruta" value="{{ old('ruta') }}" required autocomplete="ruta">
             @error('ruta')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
         <div class="form-group row">
             <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
             <div class="col-md-6">
                 <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion">
             @error('descripcion')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
             @enderror
             </div>
         </div>
        
          <div class="form-row">
          <label for="cu_id" class="col-md-4 col-form-label text-md-right">{{ __('Cu') }}</label>
               <div class="col-md-6">
                    <select class="custom-select" name="cu_id" required>
                              <option value="null">Selecionar...</option>
                              @foreach ($Cu->all() as $cu)
                                   <option value="{{$cu->id}}">{{$cu->descripcion}}</option>
                              @endforeach          
                    </select>
                    @error('cu_id')
                         <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                         </span>
                    @enderror
               </div>
          </div> 
         <!--  datos añadidos hasta aqui-->
         <div class="form-group row mb-3">
             <div class="col-md-6 offset-md-4">
              
                 <button type="submit" class="btn btn-primary">
                     {{ __('Registrar') }}
                 </button>
                 <a class="btn btn-danger col-md-2 offset-md-4" 
                 href="{{url()->previous()}}">Cancelar</a>
             </div>
         </div>
 
     </form>
 </div>

@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">

  </div>
</div>
@endsection
