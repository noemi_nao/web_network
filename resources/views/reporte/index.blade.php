@extends('layouts.layout')

@section('title','Listado de Reportes')
    
@section('content')
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Listado de Reportes</h1>
</font>
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
                <th>#</th>
                <th>DESCRIPCION</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Reporte Items Ingresados en el Mes</td>
                <td>
                    <a href="{{route('reporte.1')}}" class="btn btn-secondary">Ver mas..</a>                        
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Reporte Presupuestos Realizados en el Mes</td>
                <td>
                    <a href="{{route('reporte.2')}}" class="btn btn-secondary">Ver mas..</a>                        
                </td>
            </tr>

        </tbody>
    </table>
</div>
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection
