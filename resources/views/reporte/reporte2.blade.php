@extends('layouts.layout')

@section('title','Reporte Presupuestos')
    
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
   
@endif
<font size=32 style="color:#132735" face="Segoe UI">
    <h1 align="center">Reporte Presupuestos Realizados en el Mes</h1>
</font>
    
@empty ($vistas)
    <div class="alert alert-warning">
        No existen vistas Creados
    </div>
@else
<div class="table-responsive">
    <table class="table table-striped" >
        <thead class="thrad-light">
            <tr>
              <th>#</th>
                <th>DESCRIPCION</th>
                <th>FECHA SOLICITUD</th>
                <th>CLIENTE</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($vistas as $vista)
            <tr>
              <td>{{$vista->id}}</td>
                <td>{{$vista->descripcion}}</td>
                <td>{{$vista->fechasolicitud}}</td>
                <td>{{$vista->nombre}}</td>>              
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endempty
@endsection
@section('footer')
<div class="alert alert-primary" role="alert">
    -   
<div class="float-right d-none d-sm-inline-block">
    <b>Visitas:</b>{{$menus->vistas}}
  </div>
</div>
@endsection



