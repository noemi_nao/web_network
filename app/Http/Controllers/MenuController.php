<?php

namespace App\Http\Controllers;

use App\Modulo;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Menus =DB::table('menus')->where('nombre','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('menu.index',compact('Menus','query'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Modulo=new Modulo();
        return view('menu.create',compact('Modulo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'nombre' => 'required|max: 50',
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 200',
             
       ]);

   
        $menu =new Menu();
        $menu->nombre=$request->input('nombre');   
        $menu->ruta=$request->input('ruta');   
        $menu->descripcion=$request->input('descripcion');  
        $menu->modulo_id=$request->input('modulo_id');    
        $menu->save();
        return redirect()->route('menu.index')->with('status','menu Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Modulo=new Modulo();
        $menu = Menu::find($id);
        return view('menu.edit',compact('menu','Modulo'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData=$request->validate([
            'nombre' => 'required|max: 50',
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 200',
             
       ]);
        $menu =Menu::find($id);;
        $menu->nombre=$request->input('nombre');   
        $menu->ruta=$request->input('ruta');   
        $menu->descripcion=$request->input('descripcion');  
        $menu->Modulo_id=$request->input('Modulo_id');    
        $menu->save();
       return redirect()->route('menu.show',[$menu])->with('status','menu Modificado');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu= Menu::find($id);
         $menu->delete();
         return redirect()->route('menu.index')->with('status','menu Eliminado');;
    }
}
