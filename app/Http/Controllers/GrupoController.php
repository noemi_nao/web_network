<?php

namespace App\Http\Controllers;

use App\Grupo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class GrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Grupos =DB::table('grupos')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('grupo.index',compact('Grupos','query'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grupo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
         
            'descripcion' => 'required|max: 100',
             
       ]);

   
        $grupo =new Grupo();
        $grupo->descripcion=$request->input('descripcion');      
        $grupo->save();
        return redirect()->route('grupo.index')->with('status','grupo Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
       $grupo = Grupo::find($id);
        return view('grupo.show',compact('grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
        $grupo = Grupo::find($id);
        return view('grupo.edit',compact('grupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData=$request->validate([
            'descripcion' => 'descripcion|max: 100',       
       ]);
       $grupo = Grupo::find($id);
       $grupo->descripcion=$request->input('descripcion');
       $grupo->save();
       return redirect()->route('grupo.show',[$grupo])->with('status','grupo Modificado');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupo= Grupo::find($id);
         $grupo->delete();
         return redirect()->route('grupo.index')->with('status','grupo Eliminado');;
    }
}
