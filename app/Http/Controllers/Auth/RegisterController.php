<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\User;
use App\Cliente;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' =>['required','string','max:2555'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'sexo' => ['required','in:M,F'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],      
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

       $usuario =new User();
       $usuario->name=$data['name'];
       $usuario->last_name=$data['last_name'];
       $usuario->email=$data['email'];
       $usuario->password=Hash::make($data['password']);
       $usuario->tipo_useriario_id=2;
       $usuario->save();
    
        $cliente =new Cliente();
        $cliente->documento=$data['documento'];
        $cliente->nombre=$data['name'];
        $cliente->apellidos=$data['last_name'];
        $cliente->fechanac=$data['fechanac'];
        $cliente->sexo=$data['sexo'];
        $cliente->telefono=$data['telefono'];
        $cliente->direccion=$data['direccion'];
        $cliente->email=$data['email'];
        $cliente->users_id=$usuario->id;
        $cliente->save();
        return  $usuario;
    }
}
