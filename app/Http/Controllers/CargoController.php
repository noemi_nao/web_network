<?php

namespace App\Http\Controllers;

use App\Cargo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $cargos =DB::table('cargos')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('cargo.index',compact('cargos','query'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cargo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
         
            'descripcion' => 'required|max: 50',
            'observacion' => 'required|max: 100',
             
       ]);

   
        $cargo =new cargo();
        $cargo->descripcion=$request->input('descripcion');  
        $cargo->observacion=$request->input('observacion');      
        $cargo->save();
        return redirect()->route('cargo.index')->with('status','cargo Registrado');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
       $cargo = cargo::find($id);
        return view('cargo.show',compact('cargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
        $cargo = cargo::find($id);
        return view('cargo.edit',compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData=$request->validate([
            'descripcion' => 'required|max: 50',
            'observacion' => 'required|max: 100',
                  
       ]);
       $cargo = cargo::find($id);
       $cargo->descripcion=$request->input('descripcion');
       $cargo->observacion=$request->input('observacion');  
       $cargo->save();
       return redirect()->route('cargo.show',[$cargo])->with('status','cargo Modificado');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo= cargo::find($id);
         $cargo->delete();
         return redirect()->route('cargo.index')->with('status','cargo Eliminado');;
    }
}
