<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        
        if($request)
        {   $query=trim($request->get('search'));
            $Clientes =DB::table('clientes')->where('nombre','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('cliente.index',compact('Clientes','query'));
        }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view('cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'documento' => 'required|max: 8',
            'nombre' => 'required|max: 100',
            'apellidoP' => 'required|max: 200',
            'apellidoM' => 'required|max: 200',
            'fechanac' => 'required|max: 200',
            'telefono' => 'required|max: 8',
            'direccion' => 'required|max: 200',
        ]);

        $cliente = new Cliente();
        $cliente->documento = $request->input('documento');
        $cliente->nombre = $request->input('nombre');
        $cliente->apellidop = $request->input('apellidoP');
        $cliente->apellidom = $request->input('apellidoM');
        $cliente->fechanac = $request->input('fechanac');
        $cliente->sexo = $request->input('sexo');
        $cliente->telefono = $request->input('telefono');
        $cliente->direccion = $request->input('direccion');
        $cliente->estado = 'A';
        $cliente->save();
        return redirect()->route('cliente.index')->with('status', 'cliente Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
