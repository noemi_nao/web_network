<?php

namespace App\Http\Controllers;

use App\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        if($request)
        {
            $query=trim($request->get('search'));
            $proveedores =DB::table('proveedors')->where('nombre','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('proveedor.index',compact('proveedores','query'));
        }
 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'nombre' => 'required|max: 50',
            'direccion' => 'required|max: 200',
            'telefono' => 'required|max: 8',
            'correo' => 'required|max: 100',
            'nit' => 'required',
       ]);

        $proveedor =new Proveedor();
        $proveedor->nombre=$request->input('nombre');
        $proveedor->direccion=$request->input('direccion');
        $proveedor->estado="A";
        $proveedor->telefono=$request->input('telefono');
        $proveedor->correo=$request->input('correo');
        $proveedor->nit=$request->input('nit');
        $proveedor->save();
        return redirect()->route('proveedor.index')->with('status','Proveedor Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show(Proveedor $proveedor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit(Proveedor $proveedor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proveedor $proveedor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proveedor $proveedor)
    {
        //
    }
}
