<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Cargo;
use App\Almacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('search'));
            $Empleados = DB::table('empleados')->where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('id', 'asc')
                ->paginate(50);
            return view('empleado.index', compact('Empleados', 'query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        $Cargos = Cargo::all();
        return view('empleado.create', compact('Cargos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'documento' => 'required|max: 8',
            'nombre' => 'required|max: 100',
            'apellidoP' => 'required|max: 200',
            'apellidoM' => 'required|max: 200',
            'fechanac' => 'required|max: 200',
            'telefono' => 'required|max: 8',
            'direccion' => 'required|max: 200',
            'cargo_id' => 'required',
        ]);

        $Empleado = new Empleado();
        $Empleado->documento = $request->input('documento');
        $Empleado->nombre = $request->input('nombre');
        $Empleado->apellidop = $request->input('apellidoP');
        $Empleado->apellidom = $request->input('apellidoM');
        $Empleado->fechanac = $request->input('fechanac');
        $Empleado->sexo = $request->input('sexo');
        $Empleado->telefono = $request->input('telefono');
        $Empleado->direccion = $request->input('direccion');
        $Empleado->estado = 'A';
        $Empleado->cargo_id = $request->input('cargo_id');

        $Empleado->save();
        return redirect()->route('empleado.index')->with('status', 'Empleado Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        //
    }
}
