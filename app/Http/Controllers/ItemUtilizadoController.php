<?php

namespace App\Http\Controllers;

use App\ItemUtilizado;
use Illuminate\Http\Request;

class ItemUtilizadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemUtilizado  $itemUtilizado
     * @return \Illuminate\Http\Response
     */
    public function show(ItemUtilizado $itemUtilizado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemUtilizado  $itemUtilizado
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemUtilizado $itemUtilizado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemUtilizado  $itemUtilizado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemUtilizado $itemUtilizado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemUtilizado  $itemUtilizado
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemUtilizado $itemUtilizado)
    {
        //
    }
}
