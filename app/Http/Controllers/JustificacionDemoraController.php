<?php

namespace App\Http\Controllers;

use App\JustificacionDemora;
use Illuminate\Http\Request;

class JustificacionDemoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JustificacionDemora  $justificacionDemora
     * @return \Illuminate\Http\Response
     */
    public function show(JustificacionDemora $justificacionDemora)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JustificacionDemora  $justificacionDemora
     * @return \Illuminate\Http\Response
     */
    public function edit(JustificacionDemora $justificacionDemora)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JustificacionDemora  $justificacionDemora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JustificacionDemora $justificacionDemora)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JustificacionDemora  $justificacionDemora
     * @return \Illuminate\Http\Response
     */
    public function destroy(JustificacionDemora $justificacionDemora)
    {
        //
    }
}
