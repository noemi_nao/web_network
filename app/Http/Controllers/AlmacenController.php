<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Regional;
use App\TipoAlmacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Almacens =DB::table('almacens')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('almacen.index',compact('Almacens','query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Regionals = Regional::all();
        $TipoAlmacens = TipoAlmacen::all();
        return view('almacen.create',compact('Regionals','TipoAlmacens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     $validatedData = $request->validate([
        'descripcion' => 'required|max: 50',
        'direccion' => 'required|max: 200',
        'regional_id' => 'required',
        'tipoalmacen_id' => 'required',
    ]);
        $Almacen = new Almacen();
        $Almacen->descripcion = $request->input('descripcion');
        $Almacen->estado = 'A';
        $Almacen->direccion = $request->input('direccion');
        $Almacen->tipoalmacen_id = $request->input('tipoalmacen_id');   
        $Almacen->regional_id = $request->input('regional_id');
        $Almacen->save();
        return redirect()->route('almacen.index')->with('status', 'Almacen Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function show(Almacen $almacen)
    {
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function edit(Almacen $almacen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Almacen $almacen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Almacen $almacen)
    {
        //
    }
}
