<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\AlmacenItem;
use App\Cliente;
use App\Empleado;
use App\ItemUtilizado;
use App\JustificacionDemora;
use App\Localidad;
use App\OrdenTrabajo;
use App\TipoOrden;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdenTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('search'));
            $Ordentrabajos = DB::table('ordentrabajos')->where('id', 'LIKE', '%' . $query . '%')
                ->orderBy('id', 'asc')
                ->paginate(50);
            return view('ordentrabajo.index', compact('Ordentrabajos', 'query'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Localidads = Localidad::all();
        $TipoOrdens = TipoOrden::all();
        $Clientes = Cliente::all();
        $Empleados = Empleado::all();
        return view('ordentrabajo.create', compact('Localidads', 'TipoOrdens', 'Clientes', 'Empleados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'telefono' => 'required',
            'cliente_id' => 'required',
            'tipoorden_id' => 'required',
            'localidad_id' => 'required',
        ]);
        $date = new \DateTime();
        $Ordentrabajos = new OrdenTrabajo();
        $Ordentrabajos->telefono1 = $request->input('telefono');
        $Ordentrabajos->postacion = 0;
        $Ordentrabajos->cruce = 0;
        $Ordentrabajos->resultado = 'PENDIENTE';
        $Ordentrabajos->fechaCreacion = $date->format('Y-m-d H:i:s');
        $Ordentrabajos->fechaRecepcion = $date->format('Y-m-d H:i:s');
        $Ordentrabajos->estado = 'A';
        $Ordentrabajos->cliente_id = $request->input('cliente_id');
        $Ordentrabajos->tipoorden_id = $request->input('tipoorden_id');
        $Ordentrabajos->localidad_id = $request->input('localidad_id');
        $Ordentrabajos->usuario_id = (auth()->user()->id);
        $Ordentrabajos->save();
        return redirect()->route('ordentrabajo.index')->with('status', 'Orden trabajos Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrdenTrabajo  $ordenTrabajo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //hacer una consulta para tipo item para todos los demas y otra para cada tipo de equipos serializados

       $Orden=DB::select('SELECT * FROM ordentrabajos WHERE id='.$id.';');
       $Tipo=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, tipo_ordens AS t WHERE o.tipoorden_id=t.id AND o.id='.$id.';');
       $Justificacion=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, justificacion_demoras AS t WHERE o.justificacion_id=t.id AND o.id='.$id.';');
       $Cliente=DB::select('SELECT t.nombre,t.apellidop,t.apellidom,t.direccion,t.telefono FROM ordentrabajos AS o, clientes AS t WHERE o.cliente_id=t.id AND o.id='.$id.';');
       $Localidad=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, localidads AS t WHERE o.localidad_id=t.id AND o.id='.$id.';');
       $Ferreteria=DB::select('SELECT i.descripcion,iu.cantidad FROM ordentrabajos AS o, item_utilizados AS iu, items AS i WHERE o.id=iu.orden_id AND iu.item_id=i.id AND i.tipoitem_id<>1 AND o.id='.$id.';');
       $Equipo=DB::select('SELECT i.descripcion,iu.serie FROM ordentrabajos AS o, item_utilizados AS iu, items AS i WHERE o.id=iu.orden_id AND iu.item_id=i.id AND i.tipoitem_id=1 AND o.id='.$id.';');   
       return view('ordentrabajo.show', compact('Orden','Tipo','Justificacion','Cliente','Localidad','Ferreteria','Equipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrdenTrabajo  $ordenTrabajo
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdenTrabajo $ordenTrabajo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrdenTrabajo  $ordenTrabajo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdenTrabajo $ordenTrabajo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrdenTrabajo  $ordenTrabajo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $date = new \DateTime();
        //creando la solciitud de traspaso
        $orden = OrdenTrabajo::find($id);
       
        $orden->estado = 'C';
        $orden->resultado = 'RECHAZADA';
        $orden->fechaInicio = $date->format('Y-m-d H:i:s');
        $orden->fechaFin = $date->format('Y-m-d H:i:s');
        $orden->save();
    }

    public function asignar()
    {
        $Cuadrillas = Almacen::all();
        $Ordentrabajos =  DB::select('SELECT *FROM ordentrabajos WHERE almacen_id IS NULL');
        return view('ordentrabajo.asignar', compact('Ordentrabajos', 'Cuadrillas'));
    }

    public function asignarOT(Request $request)
    {

        $ordenes = $request->ots;
        foreach ($ordenes as $orden) {
            $ot = OrdenTrabajo::find($orden);
            $ot->almacen_id = $request->input('almacen_id');
            $ot->save();
        }
        return redirect()->route('ordentrabajo.asignar')->with('status', 'Orden trabajos Asignadas');
    }


public function cerrar($id)
{
    //obtener la solicitud colocar los datos y luego obtener lo mismo q para traspaso solo q hay q hacer la resta de otra forma
    //podemos hacer insertando en la itemsutilizado y en la de detalle items
    $Orden=DB::select('SELECT * FROM ordentrabajos WHERE id='.$id.';');
    $Tipo=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, tipo_ordens AS t WHERE o.tipoorden_id=t.id AND o.id='.$id.';');
    $Justificacion=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, justificacion_demoras AS t WHERE o.justificacion_id=t.id AND o.id='.$id.';');
    $Cliente=DB::select('SELECT t.nombre,t.apellidop,t.apellidom,t.direccion,t.telefono FROM ordentrabajos AS o, clientes AS t WHERE o.cliente_id=t.id AND o.id='.$id.';');
    $Localidad=DB::select('SELECT t.descripcion FROM ordentrabajos AS o, localidads AS t WHERE o.localidad_id=t.id AND o.id='.$id.';');
   $Demora=JustificacionDemora::all();

    $Ferreteria =  DB::select('SELECT i.id, i.descripcion, SUM(ai.cantidad) as stock
    FROM almacens as a,almacen_items as ai,items as i
    WHERE a.id=ai.almacen_id AND ai.item_id=i.id AND a.id=' . (auth()->user()->almacen_id) . ' AND i.tipoitem_id=2
    group by(i.id)');
    $Equipos = DB::table('almacens')
        ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
        ->join('items', 'almacen_items.item_id', '=', 'items.id')
        ->select('items.descripcion', 'almacen_items.serie', 'items.id')
        ->where('almacens.id', '=', (auth()->user()->almacen_id))
        ->where('items.tipoitem_id', '=', 1)
        ->where('items.marca_id', '=', 2)
        ->where('almacen_items.instalado', '=', 'F')
        ->orderBy('items.descripcion')
        ->get();

    $ztes = DB::table('almacens')
        ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
        ->join('items', 'almacen_items.item_id', '=', 'items.id')
        ->select('items.descripcion', 'almacen_items.serie', 'items.id')
        ->where('almacens.id', '=', (auth()->user()->almacen_id))
        ->where('items.tipoitem_id', '=', 1)
        ->where('items.marca_id', '=', 3)
        ->where('almacen_items.instalado', '=', 'F')
        ->orderBy('items.descripcion')
        ->get();

    $iptvs = DB::table('almacens')
        ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
        ->join('items', 'almacen_items.item_id', '=', 'items.id')
        ->select('items.descripcion', 'almacen_items.serie', 'items.id')
        ->where('almacens.id', '=', (auth()->user()->almacen_id))
        ->where('items.tipoitem_id', '=', 1)
        ->where('items.marca_id', '=', 4)
        ->where('almacen_items.instalado', '=', 'F')
        ->orderBy('items.descripcion')
        ->get();
     
    return view('ordentrabajo.cerrar', compact('Orden','Tipo','Demora','Justificacion','Cliente','Localidad','Ferreteria', 'Equipos', 'ztes', 'iptvs'));
}

public function cerrarOT(Request $request)
    {
        $mensaje = 'solicitud no Enviada';
        try {
            DB::beginTransaction();
            $date = new \DateTime();
            //creando la solciitud de traspaso
            $orden = OrdenTrabajo::find($request->input('idorden'));
           
            $orden->estado = 'C';
            $orden->resultado = 'EJECUTADO';
            $orden->fechaInicio = $date->format('Y-m-d H:i:s');
            $orden->fechaFin = $date->format('Y-m-d H:i:s');
            $orden->observacion = $request->input('observacion');
            $orden->justificacion_id = $request->input('iddemora');;
            $orden->save();

            $id = $request->input('idorden');
            //ferreteria
            $idarticulo = $request->get('idarticulo');
            $cantidad = $request->get('cantidad');
            //equipos
            $huaweis = $request->huawei;
            $ztes = $request->zte;
            $iptvs = $request->iptv;

         
            $cont = 0;
            while ($cont < count($idarticulo)) {
            
                //cargando el detalle de la
                    $itemUtilizado = new ItemUtilizado();
                    $itemUtilizado->cantidad = $cantidad[$cont];
                    $itemUtilizado->orden_id = $id;
                    $itemUtilizado->item_id =$idarticulo[$cont];
                    $itemUtilizado->save();

                    //quitar el material al almacen origen
                    $almacenItem = new AlmacenItem();
                    $almacenItem->cantidad = $cantidad[$cont] * -1;
                    $almacenItem->instalado = 'F';
                    $almacenItem->almacen_id = (auth()->user()->almacen_id);
                    $almacenItem->item_id = $idarticulo[$cont];
                    $almacenItem->save();
                    $cont = $cont + 1;
            }
            $mensaje = 'ferreteria insertada';
            $cont = 0;
            if ($huaweis <> null) {
                foreach ($huaweis as $huawei) {
                    //cargando el detalle de la
                    $itemUtilizado = new ItemUtilizado();
                    $itemUtilizado->serie = $huawei;
                    $itemUtilizado->cantidad = 1;
                    $itemUtilizado->orden_id = $id;
                    $itemUtilizado->item_id = 4;
                    $itemUtilizado->save();
                    
                    //quitar el material al almacen origen   

                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $huawei)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'I']);

                    $cont = $cont + 1;
                }
            }
            $mensaje = 'huawei insertada';
            //equipos zte3,huawe4,iptv5
            $cont = 0;
            if ($ztes <> null) {
                foreach ($ztes as $zte) {
                    //cargando el detalle de la
                    
                    $itemUtilizado = new ItemUtilizado();
                    $itemUtilizado->serie = $zte;
                    $itemUtilizado->cantidad = 1;
                    $itemUtilizado->orden_id = $id;
                    $itemUtilizado->item_id = 3;
                    $itemUtilizado->save();
                               
                    //quitar el material al almacen origen 

                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $zte)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'I']);

                    $cont = $cont + 1;
                }
            }
            $mensaje = 'zte insertada';
            //equipos zte3,huawe4,iptv5
            $cont = 0;
            if ($iptvs <> null) {
                foreach ($iptvs as $iptv) {
                    //cargando el detalle de la
                    $itemUtilizado = new ItemUtilizado();
                    $itemUtilizado->serie = $iptv;
                    $itemUtilizado->cantidad = 1;
                    $itemUtilizado->orden_id = $id;
                    $itemUtilizado->item_id = 5;
                    $itemUtilizado->save();
                    //quitar el material al almacen origen            
                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $iptv)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'I']);
                    $cont = $cont + 1;
                }
            }
            DB::commit();
            $mensaje = 'ot cerrada';
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect()->route('ordentrabajo.index')->with('status',  $mensaje);
      
      
    }
}