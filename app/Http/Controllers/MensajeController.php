<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MensajeController extends Controller
{
    public function index()
    {   
     
            $this->addCountVisit();           
            return view('mensaje.index');    
        
    }
    private function addCountVisit(){
        Auth::user()->countPage(10);
    }
}
