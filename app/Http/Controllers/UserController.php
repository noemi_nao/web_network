<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Empleado;
use App\Grupo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        if($request)
        {
            $query=trim($request->get('search'));
            $users =DB::table('users')->where('email','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('user.index',compact('users','query'));
        }
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $Grupos = Grupo::all(); 
        $Empleados =  Empleado::all(); 
        $Almacens = Almacen::all();
        return view('user.create',compact('Grupos','Empleados','Almacens'));     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validatedData=$request->validate([
            'email' => 'required|max: 50',
            'foto' => 'required|image',   
       ]);
       if($request->hasFile('foto')){
        $file =$request->file('foto');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/images/usuarios/',$name);
         }
        $usuario =new user();
        $usuario->email=$request->input('email');
        $usuario->password=Hash::make($request->input('password'));
        $usuario->empleado_id=$request->input('empleado_id');
        $usuario->grupo_id=$request->input('grupo_id');
        $usuario->imagen=$name;
        if ($request->get('almacen_id') != 'null') {
            $usuario->almacen_id = $request->input('almacen_id');
        }
        $usuario->name='USUARIO';
        $usuario->save();
     
        return redirect()->route('usuarios.index')->with('status','Usuario Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id){ 
        $user = user::find($id);  
        return view('user.edit',compact('user')); 
      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $validatedData=$request->validate([
            'email' => 'required|max: 50', 
       ]);
       $user = user::find($id);
       $user->fill($request->except('foto'));
             if($request->hasFile('foto')){
                 $file =$request->file('foto');
                 $name=time().$file->getClientOriginalName();
                 $user->foto=$name; 
                 $file->move(public_path().'/images/usuarios/',$name);
            } 
       $user->password=Hash::make($request->input('password'));
       $user->save();
       return redirect()->route('usuarios.index')->with('status','usuario Modificado');
      }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user){
        $usuario = User::findOrFail($user);
        $usuario->estado='false';
        $usuario->save();
          return redirect()->route('usuarios.index')
          ->withSuccess("EL usuario fue dado de baja");  
      }

      public function change($color){
        $my_id=(auth()->user()->id);
        $estilo=User::find($my_id);
        $estilo->estilo=$color;
        $estilo->save();
        return redirect()->route('home');
    }
    public function changeFont($tamaño){
        $my_id=(auth()->user()->id);
        $font=User::find($my_id);
        $font->font=$tamaño;
        $font->save();
        return redirect()->route('home');
    }
}
