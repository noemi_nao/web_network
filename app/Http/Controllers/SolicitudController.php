<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\AlmacenItem;
use App\DetalleSolicitud;
use App\Item;
use App\solicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('search'));
            $Solicituds = DB::table('solicituds')->where('id', 'LIKE', '%' . $query . '%')
                ->orderBy('id', 'asc')
                ->paginate(50);
            return view('solicitud.index', compact('Solicituds', 'query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $items = DB::table('items')
            ->join('proveedors', 'items.proveedor_id', '=', 'proveedors.id')
            ->select('items.id as id', 'items.descripcion as descripcion', 'proveedors.nombre as nombre')
            ->where('tipoitem_id', '=', 2)
            ->get();
        return view('solicitud.create', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mensaje = 'solicitud no Enviada';
        try {
            DB::beginTransaction();
            $date = new \DateTime();
            $solicituds = new Solicitud();
            $solicituds->descripcion = $request->get('descripcion');;
            $solicituds->estado = 'P';
            $solicituds->fechasolicitud = $date->format('Y-m-d H:i:s');
            $solicituds->almacendestino_id = (auth()->user()->almacen_id);
            $solicituds->tiposolicitud_id = 1;
            $solicituds->usuario_id = (auth()->user()->id);
            $solicituds->save();

            $id = $solicituds->id;
            $idarticulo = $request->get('idarticulo');
            $cantidad = $request->get('cantidad');
            $precio = $request->get('precio');

            $cont = 0;
            while ($cont < count($idarticulo)) {
                $detallesolicitud = new DetalleSolicitud();
                $detallesolicitud->estado = 'N';
                $detallesolicitud->cantidad = $cantidad[$cont];
                $detallesolicitud->precio = $precio[$cont];
                $detallesolicitud->instalado = 'N';
                $detallesolicitud->solicitud_id = $id;
                $detallesolicitud->item_id = $idarticulo[$cont];

                $detallesolicitud->save();
                $cont = $cont + 1;
            }

            DB::commit();
            $mensaje = 'solicitud Enviada';
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect()->route('solicitud.index')->with('status', $mensaje);
    }

    public function ingreso()
    {
        $almacens = DB::table('almacens')
            ->select('almacens.id as id', 'almacens.descripcion as descripcion')
            ->where('almacens.tipoalmacen_id', '=', 1)
            ->get();

        $items = DB::table('items')
            ->join('proveedors', 'items.proveedor_id', '=', 'proveedors.id')
            ->select('items.id as id', 'items.descripcion as descripcion', 'proveedors.nombre as nombre')
            ->where('tipoitem_id', '=', 1)
            ->get();
        return view('solicitud.ingreso', compact('items', 'almacens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ingresostore(Request $request)
    {
        $mensaje = 'ingreso no realizado';
        try {
            DB::beginTransaction();
            $date = new \DateTime();
            $solicituds = new Solicitud();
            $solicituds->descripcion = 'INGRESO DE EQUIPOS';
            $solicituds->estado = 'A';
            $solicituds->fechasolicitud = $date->format('Y-m-d H:i:s');
            $solicituds->fechaaprobacion = $date->format('Y-m-d H:i:s');
            $solicituds->almacendestino_id = $request->get('almacen_id');
            $solicituds->tiposolicitud_id = 4;
            $solicituds->usuario_id = (auth()->user()->id);
            $solicituds->save();

            $id = $solicituds->id;
            $idarticulo = $request->get('idarticulo');
            $cantidad = $request->get('cantidad');

            $cont = 0;
            while ($cont < count($idarticulo)) {
                $detallesolicitud = new DetalleSolicitud();
                $detallesolicitud->serie = $cantidad[$cont];
                $detallesolicitud->estado = 'N';
                $detallesolicitud->cantidad = 1;
                $detallesolicitud->instalado = 'N';
                $detallesolicitud->precio = 0;
                $detallesolicitud->solicitud_id = $id;
                $detallesolicitud->item_id = $idarticulo[$cont];
                $detallesolicitud->save();

                $almacenItems = new AlmacenItem();
                $almacenItems->serie = $cantidad[$cont];
                $almacenItems->cantidad = 1;
                $almacenItems->almacen_id = $request->get('idalmacen');
                $almacenItems->item_id = $idarticulo[$cont];
                $almacenItems->instalado = 'F';
                $almacenItems->save();
                $cont = $cont + 1;
            }

            DB::commit();
            $mensaje = 'ingreso realizado';
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect()->route('solicitud.index')->with('status', $mensaje);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Total = DB::select('SELECT SUM(cantidad*precio) as total FROM detalle_solicituds WHERE solicitud_id=' . $id . ';');

        $Solicituds = DB::table('solicituds')
            ->join('detalle_solicituds', 'solicituds.id', '=', 'detalle_solicituds.solicitud_id')
            ->join('items', 'detalle_solicituds.item_id', '=', 'items.id')
            ->select('solicituds.id', 'items.descripcion', 'detalle_solicituds.precio', 'detalle_solicituds.cantidad')
            ->where('solicituds.id', '=', $id)
            ->get();
        return view('solicitud.show', compact('Solicituds', 'Total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mensaje = 'solicitud no aprobada';
        try {
            DB::beginTransaction();
            $detalles = DB::table('solicituds')
                ->join('detalle_solicituds', 'solicituds.id', '=', 'detalle_solicituds.solicitud_id')
                ->select('detalle_solicituds.id as id', 'detalle_solicituds.item_id as item', 'solicituds.almacendestino_id as almacen', 'detalle_solicituds.cantidad as cantidad')
                ->where('detalle_solicituds.solicitud_id', '=', $id)
                ->get();
            foreach ($detalles as $detalle) {
                $almacenItems = new AlmacenItem();
                $almacenItems->cantidad = $detalle->cantidad;
                $almacenItems->almacen_id = $detalle->almacen;
                $almacenItems->item_id = $detalle->item;
                $almacenItems->instalado = 'F';
                $almacenItems->save();
            }
            $date = new \DateTime();
            $solicitud = solicitud::find($id);
            $solicitud->estado = 'A';
            $solicitud->fechaaprobacion = $date->format('Y-m-d H:i:s');
            $solicitud->save();
            DB::commit();
            $mensaje = 'solicitud aprobada';
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect()->route('solicitud.index')->with('status', $mensaje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, solicitud $solicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $date = new \DateTime();
        $solicitud = solicitud::find($id);
        $solicitud->estado = 'R';
        $solicitud->fechaaprobacion = $date->format('Y-m-d H:i:s');
        $solicitud->save();
        return redirect()->route('solicitud.index')->with('status', 'Solicitud rechazada');
    }

    public function traspaso()
    {
        $Almacens = Almacen::all();
        $Ferreteria =  DB::select('SELECT i.id, i.descripcion, SUM(ai.cantidad) as stock
        FROM almacens as a,almacen_items as ai,items as i
        WHERE a.id=ai.almacen_id AND ai.item_id=i.id AND a.id=' . (auth()->user()->almacen_id) . ' AND i.tipoitem_id=2
        group by(i.id)');
        $Equipos = DB::table('almacens')
            ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
            ->join('items', 'almacen_items.item_id', '=', 'items.id')
            ->select('items.descripcion', 'almacen_items.serie', 'items.id')
            ->where('almacens.id', '=', (auth()->user()->almacen_id))
            ->where('items.tipoitem_id', '=', 1)
            ->where('items.marca_id', '=', 2)
            ->where('almacen_items.instalado', '=', 'F')
            ->orderBy('items.descripcion')
            ->get();

        $ztes = DB::table('almacens')
            ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
            ->join('items', 'almacen_items.item_id', '=', 'items.id')
            ->select('items.descripcion', 'almacen_items.serie', 'items.id')
            ->where('almacens.id', '=', (auth()->user()->almacen_id))
            ->where('items.tipoitem_id', '=', 1)
            ->where('items.marca_id', '=', 3)
            ->where('almacen_items.instalado', '=', 'F')
            ->orderBy('items.descripcion')
            ->get();

        $iptvs = DB::table('almacens')
            ->join('almacen_items', 'almacens.id', '=', 'almacen_items.almacen_id')
            ->join('items', 'almacen_items.item_id', '=', 'items.id')
            ->select('items.descripcion', 'almacen_items.serie', 'items.id')
            ->where('almacens.id', '=', (auth()->user()->almacen_id))
            ->where('items.tipoitem_id', '=', 1)
            ->where('items.marca_id', '=', 4)
            ->where('almacen_items.instalado', '=', 'F')
            ->orderBy('items.descripcion')
            ->get();
        return view('solicitud.traspaso', compact('Ferreteria', 'Almacens', 'Equipos', 'ztes', 'iptvs'));
    }

    public function traspasoItem(Request $request)
    {
        $mensaje = 'solicitud no Enviada';
        try {
            DB::beginTransaction();
            $date = new \DateTime();
            //creando la solciitud de traspaso
            $solicituds = new Solicitud();
            $solicituds->descripcion = 'TRASPASO DE ITEMS';
            $solicituds->estado = 'A';
            $solicituds->fechasolicitud = $date->format('Y-m-d H:i:s');
            $solicituds->fechaaprobacion = $date->format('Y-m-d H:i:s');
            $solicituds->almacenorigen_id = (auth()->user()->almacen_id);
            $solicituds->almacendestino_id = $request->get('almacen_id');
            $solicituds->tiposolicitud_id = 2;
            $solicituds->usuario_id = (auth()->user()->id);
            $solicituds->save();

            $id = $solicituds->id;
            //ferreteria
            $idarticulo = $request->get('idarticulo');
            $cantidad = $request->get('cantidad');
            //equipos
            $huaweis = $request->huawei;
            $ztes = $request->zte;
            $iptvs = $request->iptv;

         
            $cont = 0;
            while ($cont < count($idarticulo)) {
            
                //cargando el detalle de la
                    $detallesolicitud = new DetalleSolicitud();
                    $detallesolicitud->estado = 'N';
                    $detallesolicitud->cantidad = $cantidad[$cont];
                    $detallesolicitud->precio = 0;
                    $detallesolicitud->instalado = 'N';
                    $detallesolicitud->solicitud_id = $id;
                    $detallesolicitud->item_id =$idarticulo[$cont];
                    $detallesolicitud->save();

                    //asignar el material al almacen destino

                    $almacenItems = new AlmacenItem();
                    $almacenItems->cantidad = $cantidad[$cont];
                    $almacenItems->instalado = 'F';
                    $almacenItems->almacen_id = $request->get('almacen_id');
                    $almacenItems->item_id = $idarticulo[$cont];
                    $almacenItems->save();
                    $mensaje = 'solicitud creada';
                    //quitar el material al almacen origen
                    $almacenItem = new AlmacenItem();
                    $almacenItem->cantidad = $cantidad[$cont] * -1;
                    $almacenItem->instalado = 'F';
                    $almacenItem->almacen_id = (auth()->user()->almacen_id);
                    $almacenItem->item_id = $idarticulo[$cont];
                    $almacenItem->save();
                    $cont = $cont + 1;
            }
            $mensaje = 'ferreteria insertada';
            $cont = 0;
            if ($huaweis <> null) {
                foreach ($huaweis as $huawei) {
                    //cargando el detalle de la
                    $detallesolicitud = new DetalleSolicitud();
                    $detallesolicitud->estado = 'N';
                    $detallesolicitud->serie = $huawei;
                    $detallesolicitud->cantidad = 1;
                    $detallesolicitud->precio = 0;
                    $detallesolicitud->instalado = 'N';
                    $detallesolicitud->solicitud_id = $id;
                    $detallesolicitud->item_id = 4;
                    $detallesolicitud->save();
                    //asignar el material al almacen destino
                    $almacenItems = new AlmacenItem();
                    $almacenItems->serie = $huawei;
                    $almacenItems->cantidad = 1;
                    $almacenItems->almacen_id = $request->get('almacen_id');
                    $almacenItems->item_id = 4;
                    $almacenItems->instalado = 'F';
                    $almacenItems->save();
                    //quitar el material al almacen origen   

                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $huawei)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'T']);

                    $cont = $cont + 1;
                }
            }
            $mensaje = 'huawei insertada';
            //equipos zte3,huawe4,iptv5
            $cont = 0;
            if ($ztes <> null) {
                foreach ($ztes as $zte) {
                    //cargando el detalle de la
                    $detallesolicitud = new DetalleSolicitud();
                    $detallesolicitud->estado = 'N';
                    $detallesolicitud->serie = $zte;
                    $detallesolicitud->cantidad = 1;
                    $detallesolicitud->precio = 0;
                    $detallesolicitud->instalado = 'N';
                    $detallesolicitud->solicitud_id = $id;
                    $detallesolicitud->item_id = 3;
                    $detallesolicitud->save();
                    //asignar el material al almacen destino
                    $almacenItems = new AlmacenItem();
                    $almacenItems->serie = $zte;
                    $almacenItems->cantidad = 1;
                    $almacenItems->almacen_id = $request->get('almacen_id');
                    $almacenItems->item_id = 3;
                    $almacenItems->instalado = 'F';
                    $almacenItems->save();
                    //quitar el material al almacen origen            
                    //quitar el material al almacen origen 

                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $zte)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'T']);

                    $cont = $cont + 1;
                }
            }
            $mensaje = 'zte insertada';
            //equipos zte3,huawe4,iptv5
            $cont = 0;
            if ($iptvs <> null) {
                foreach ($iptvs as $iptv) {
                    //cargando el detalle de la
                    $detallesolicitud = new DetalleSolicitud();
                    $detallesolicitud->estado = 'N';
                    $detallesolicitud->serie = $iptv;
                    $detallesolicitud->cantidad = 1;
                    $detallesolicitud->precio = 0;
                    $detallesolicitud->instalado = 'N';
                    $detallesolicitud->solicitud_id = $id;
                    $detallesolicitud->item_id = 5;
                    $detallesolicitud->save();
                    //asignar el material al almacen destino
                    $almacenItems = new AlmacenItem();
                    $almacenItems->serie = $iptv;
                    $almacenItems->cantidad = 1;
                    $almacenItems->almacen_id = $request->get('almacen_id');
                    $almacenItems->item_id = 5;
                    $almacenItems->instalado = 'F';
                    $almacenItems->save();
                    //quitar el material al almacen origen            
                    $affected = DB::table('almacen_items')
                        ->where('almacen_items.serie', '=', $iptv)
                        ->where('almacen_items.almacen_id', '=', (auth()->user()->almacen_id))
                        ->update(['instalado' => 'T']);
                    $cont = $cont + 1;
                }
            }
            DB::commit();
            $mensaje = 'solicitud Enviada';
        } catch (\Exception $e) {
            DB::rollback();
        }

        return redirect()->route('solicitud.index')->with('status',  $mensaje);
    }
}
