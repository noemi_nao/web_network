<?php

namespace App\Http\Controllers;

use App\TipoOrden;
use Illuminate\Http\Request;

class TipoOrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoOrden  $tipoOrden
     * @return \Illuminate\Http\Response
     */
    public function show(TipoOrden $tipoOrden)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoOrden  $tipoOrden
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoOrden $tipoOrden)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoOrden  $tipoOrden
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoOrden $tipoOrden)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoOrden  $tipoOrden
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoOrden $tipoOrden)
    {
        //
    }
}
