<?php

namespace App\Http\Controllers;

use App\controlcalidad;
use Illuminate\Http\Request;

class ControlcalidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\controlcalidad  $controlcalidad
     * @return \Illuminate\Http\Response
     */
    public function show(controlcalidad $controlcalidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\controlcalidad  $controlcalidad
     * @return \Illuminate\Http\Response
     */
    public function edit(controlcalidad $controlcalidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\controlcalidad  $controlcalidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, controlcalidad $controlcalidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\controlcalidad  $controlcalidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(controlcalidad $controlcalidad)
    {
        //
    }
}
