<?php

namespace App\Http\Controllers;

use App\Item;
use App\Marca;
use App\Medida;
use App\Proveedor;
use App\TipoItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        if($request) 
        {
            $query=trim($request->get('search'));
            $items =DB::table('items')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('item.index',compact('items','query'));
        }
 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
              $proveedors = Proveedor::all();
              $tipoitems =TipoItem::all();
              $marcas =Marca::all();
              $medidas =Medida::all();
        return view('item.create',compact('tipoitems','proveedors','marcas','medidas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'umbral' => 'required',
            'observacion' => 'required|max: 50',
            'descripcion' => 'required|max: 200',
            'imagen' => 'required|image',   
       ]);

       if($request->hasFile('imagen')){
            $file =$request->file('imagen');
            $name=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/items/',$name);
       }
       
        $item =new Item(); 
    
        $item->descripcion=$request->input('descripcion');
        $item->observacion=$request->input('observacion');
        $item->umbral=$request->input('umbral');
        $item->estado= 'A';
        $item->tipoitem_id=$request->input('tipoitem_id');
        $item->proveedor_id=$request->input('proveedor_id');
        $item->marca_id=$request->input('marca_id');
        $item->medida_id=$request->input('medida_id');
        $item->foto= $name;
        $item->save();
        return redirect()->route('item.index')->with('status','item Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}
