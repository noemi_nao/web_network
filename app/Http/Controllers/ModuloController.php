<?php

namespace App\Http\Controllers;

use App\Modulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class moduloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Modulos =DB::table('modulos')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('modulo.index',compact('Modulos','query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modulo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 50',       
       ]);

        $modulo =new Modulo();
        $modulo->descripcion=$request->input('descripcion');   
        $modulo->ruta=$request->input('ruta');  
        $modulo->save();
        return redirect()->route('modulo.index')->with('status','modulo Registrado');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {$modulo = Modulo::find($id);
        return view('modulo.show',compact('modulo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modulo = Modulo::find($id);
        return view('modulo.edit',compact('modulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData=$request->validate([
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 50',       
       ]);

       $modulo =Modulo::find($id);
       $modulo->descripcion=$request->input('descripcion');   
       $modulo->ruta=$request->input('ruta');  
       $modulo->save();
       return redirect()->route('modulo.show',[$modulo])->with('status','modulo Modificado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modulo= Modulo::find($id);
         $modulo->delete();
         return redirect()->route('modulo.index')->with('status','modulo Eliminado');;
    }
}
