<?php

namespace App\Http\Controllers;

use App\TipoAlmacen;
use Illuminate\Http\Request;

class TipoAlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoAlmacen  $tipoAlmacen
     * @return \Illuminate\Http\Response
     */
    public function show(TipoAlmacen $tipoAlmacen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoAlmacen  $tipoAlmacen
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoAlmacen $tipoAlmacen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoAlmacen  $tipoAlmacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoAlmacen $tipoAlmacen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoAlmacen  $tipoAlmacen
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoAlmacen $tipoAlmacen)
    {
        //
    }
}
