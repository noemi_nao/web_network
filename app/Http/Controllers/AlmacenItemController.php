<?php

namespace App\Http\Controllers;

use App\AlmacenItem;
use Illuminate\Http\Request;

class AlmacenItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AlmacenItem  $almacenItem
     * @return \Illuminate\Http\Response
     */
    public function show(AlmacenItem $almacenItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AlmacenItem  $almacenItem
     * @return \Illuminate\Http\Response
     */
    public function edit(AlmacenItem $almacenItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AlmacenItem  $almacenItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AlmacenItem $almacenItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AlmacenItem  $almacenItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlmacenItem $almacenItem)
    {
        //
    }
}
