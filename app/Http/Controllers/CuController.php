<?php

namespace App\Http\Controllers;

use App\Cu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Cus =DB::table('cus')->where('descripcion','LIKE','%'.$query.'%')
            ->orderBy('id','asc')
            ->paginate(50);
            return view('cu.index',compact('Cus','query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 50',       
       ]);

        $Cu =new Cu();
        $Cu->descripcion=$request->input('descripcion');   
        $Cu->ruta=$request->input('ruta');  
        $Cu->save();
        return redirect()->route('cu.index')->with('status','cu Registrado');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Cu  $cu
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {$Cu = Cu::find($id);
        return view('cu.show',compact('Cu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cu  $cu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cu = Cu::find($id);
        return view('cu.edit',compact('cu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cu  $cu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData=$request->validate([
            'ruta' => 'required|max: 100',
            'descripcion' => 'required|max: 50',       
       ]);

       $Cu =Cu::find($id);
       $Cu->descripcion=$request->input('descripcion');   
       $Cu->ruta=$request->input('ruta');  
       $Cu->save();
       return redirect()->route('cu.show',[$Cu])->with('status','cu Modificado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cu  $cu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cu= Cu::find($id);
         $cu->delete();
         return redirect()->route('cu.index')->with('status','cu Eliminado');;
    }
}
