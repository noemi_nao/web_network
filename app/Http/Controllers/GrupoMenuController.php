<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\GrupoMenu;
use App\Menu;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GrupoMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request) 
        {   $query=trim($request->get('search'));
            $Grupomenus =DB::table('grupos as g')
            ->join('grupo_menus as gm','g.id','gm.grupo_id')
            ->join('menus as m','gm.menu_id','m.id')
            ->select('gm.id as id', 'g.descripcion as descripcion', 'm.nombre as nombre','m.ruta as ruta')
            ->get();
            return view('grupomenu.index',compact('Grupomenus','query'));
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Menu=new Menu();
        $Grupo=new Grupo();
        return view('grupomenu.create',compact('Menu','Grupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $Grupomenu =new GrupoMenu();
        $Grupomenu->estado='A'; 
        $Grupomenu->grupo_id=$request->input('grupo_id');   
        $Grupomenu->menu_id=$request->input('menu_id');        
        $Grupomenu->save();
        return redirect()->route('grupomenu.index')->with('status','grupomenu Registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GrupoMenu  $grupoMenu
     * @return \Illuminate\Http\Response
     */
    public function show(GrupoMenu $grupoMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GrupoMenu  $grupoMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(GrupoMenu $grupoMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GrupoMenu  $grupoMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GrupoMenu $grupoMenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GrupoMenu  $grupoMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(GrupoMenu $grupoMenu)
    {
        //
    }
}
