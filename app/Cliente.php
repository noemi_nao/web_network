<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable =['documento','nombre','apellidoP','apellidoM','fechanac','sexo','telefono','direccion','estado'];  
     
}
