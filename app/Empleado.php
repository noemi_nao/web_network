<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable =['documento','nombre','apellidoP','apellidoM','fechanac','sexo','telefono','direccion','estado'];  
    public function almacens(){
        return $this->belongsTo('App\Almacen','almacen_id','id');
    }
    public function cargos(){
        return $this->belongsTo('App\Cargo','cargo_id','id');
    }
}
