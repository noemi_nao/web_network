<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{   use Notifiable;
    protected $fillable = [
     
        'email',
        'estado',
        'estilo',
        'fuente',
        'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getUser($query)
    {
        return User::where('email', 'LIKE', '%'. $query .'%')
                ->orderBy('id', 'asc')
                ->get();
    }
}
